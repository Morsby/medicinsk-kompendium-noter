# Gastroenterologi og hepatologi

## Cirrhosis hepatis

### Generelt
Cirrose medfører dels en ændring af hepatisk hæmodynamik (dvs. *portal hypertension*), dels en reduktion af leverens metaboliske kapacitet. Dette medfører komplikationer.

Sekundært til portal hypertension optræder (med faldende hyppighed)

- ascites med hepatisk nefropati
- øsofagusvaricer med hæmatemese -- hyperdynamisk kredsløb
- kardiomyopati
- hepatopulmonalt syndrom

Sekundært til metabolisk derangement:

- hepatisk encefalopati
- malnutrition
- immundefekt

Endvidere øget risiko for *hepatocullulært carcinom*

#### Definition, karakteristik, klassifikation
**Definition**: Fibrose, der omgiver regenerationsknuder i leveren med ophævelse af den normale vævsarkitektur. Kan dog også stilles ved kliniske tegn:

- \> 5 spidernævi
- ascites
- øsofagusvaricer
- nedsatte eksterne koagulationsfaktorer

*samt*

- ultralyd (puklet leveroverflade).

Splenomegali hos cirroseptt. tyder på portal hypertension.

#### Forekomst, epidemiologi
Relativt almindelig. Incidens 1.700/år i DK. Prævalens ca. 12.000. Alle aldersgrupper afhængig af ætiologi, hyppigst omkring 60 år. Hyppigst mænd, dog afhængig af ætiologi.

#### Ætiologi
Med aftagende hyppighed

- alkoholrelaterede (50%)
- posthepatisk (viral) cirrose
- cirrose ved leversygdomme med autoimmunitet
- fedt-cirrose (tidl. nonbiliær nonalkoholsk)
- cirrose pga. arvelige sygdomme
- uklassificeret (lille rest)

#### Patogenese
Enhver inflammatorisk reaktion medførende 1) acceleret hepatocytdød, 2) hepatocytregeneration og 3) fibrogenese i længere tid, resulterende i forstyrret arkitektur (se definition). *Er irreversibel*.

#### Patologisk anatomi
S. 1622.

#### Symptomer og kliniske fund
Veklsende af art og intensitet. Ofte asymptomatisk. 

Generelle symptomer i 2/3 af tilfælde:

- mental og muskulær træthed
- nedsat initiativ
- nedsat appetit
- nedsat libido
- lægkramper

##### Typiske kliniske fund
Objektiv undersøgelse vil med stor sikkerhed diagnosticere cirrose hos 3/4 af ptt.:

- muskeltab (75%) - evt. skjult af fedtpolstre el. væskeophobning
- ascites (60%)
- variceblødning (15%)
- hepatisk encefalopati (5%)
	- Hepatisk foetor ex ore (lugt af gammelt hø)
	- forlænget latenstid i samtale
	- flappting (intermitterende tonustab ved fremstrakte arme med ekstenderede håndled)
	- trombone-tegn (intermitterende tonustab ved fremstrakt tunge)
	- rumlig desorentering (påklædning, spisning, toiletbesøg)
- infektioner (5%)
- icterus (< 15%)
- hudforandringer: *Cirrose stigmata* (**fed** stærkest korreleret)
	- **spider nævi** (av-fistler, hyppigst thoraxforside)
	- hvide negle
	- cirroseteint
	- **palmart erytem**
	- laklæber
	- **distenderede periumbilikalvener** (caput Medusa) og flankevener
	- nedsat kropsbehåring

Lever er palpabel i 2/3 af tilfælde. Ca. 50% let ømme.

Splenomegali + cirrosestigmata stærkt indicerende for portal hypertension.

Patienter

- er dyskrine (nedsat androgen- og øget østrogenspejl)
- er muskelsvage (pga. nedsat muskelmasse samt myopati)
- har kardiomypati
- har hyperdynamisk kredsløb (nedsat TPR), tendens til *arteriel hypotension*, kræver kompensatorisk højt cardiac output.
- har hepatopulmonalt syndrom (5-10%) => klager over åndenød, kronisk hypoxæmi

##### Ætiologiske clues

- xanthelasmata: primær biliær cirrose
- Kayser-Fleischer ringe: Wilsons sygdom

##### Anamnestisk information
- Toksisk
	- Alkohol
	- Medicin (herunder håndkøb og natur)
- Eksposition for hepatitis B og C
	- Blodtransfusioner før 1992 i DK/alle i udland
	- Medicinsk behandling i udland
	- Udlandsrejser
	- Seksuel adfærd
	- i.v. misbrug, tatovering, piercing, akupunktur
- Komorbiditet
	- Samtidig inflammatorisk tarmsygdom (som ved PSC)
	- metabolisk syndrom (fedt-cirrose)
- Andre autoimmune manifestationer hos pt. og familie.

#### Parakliniske fund
- Forhøjede aminotransferaser og basiske fosfataser typisk pga. grundsygdom (kan være normale!).
- Reducerede koagulationsfaktorer (II, VII, X)
- Nedsat P-Albumin tyder på længerevarende sygdom (udtryk fornedsat proteinsyntese og *malnutrition*)
- Forhøjet P-Bilirubin, altid konjugeret. Hyppighed stiger med tid.
- Hyperimmunglobulinæmi
	- IgG forhøjet hos de fleste
	- Alkoholisk: IgA
	- Autoimmun: IgG udtalt forhøjet
	- Primær biliær cirrose: IgM
	- Sjælden autoimmun cholangitis: IgG4

##### Blodbillede

- asymptomatiske
	- normal hæmoglobin, leukocytter og trombocyttter
- symptomatiske
	- normocytær, normokrom anæmi
		- ved alkoholisk dog makrocytær; ved blødning jernmangel (hypokrom, mikrocytær); meget udtalt *neutrocytose*
	- let nedsat leukocyt- og **trombocyttal**
	- hvis splenomegali kan *hyperspleni* forekomme (pancytopeni)

##### Ultralyd
Puklet leveroverflade, groft ekkomønster. Evt. bagvendt strøm i v. portae. *Op til 25% har trombose i v. portae*. Portosystemiske kollateraler og splenomegali udtryk for portal hypertension.

#### Forløb og prognose
**Inkompensation** betegnes et forløb med forekomst af mindst én komplikation og har markant forringet prognose.

- Kronisk leversvigt: langsomt progredierende tab af leverfunktion og komplikationer til portal hypertension
- Akut-i-kronisk leversvigt: Reversibel (men akut dødelig), svær forringelse af tilstand < 72 timer med
	- identificeret ætiologi
	- højt Child-Pugh score
	- P-Bilirubin > 85 mikromol/L

Påvirkes af comorbid sygdom (50%). Hyppigst hjerte-kar, lungesygdom, diabetes.

\> 1/3 døde inden for første år, >50% døde inden for 5 år. I kompenserede tilfælde er median overlevelse omkring 8 år.

##### Hyppigste dødsårsager

- Første år: Akut-i-kronisk leversvigt
	- Icterus
	- Encefalopati
	- Sepsis
	- Variceblødning
	- Hepatisk nefropati
- Senere: Kronisk leversvigt
	-  Kakeksi, svigtende almentilstand
	-  Hjertekarsygdomme
	-  Kræft (> 50 gange øget risiko for hepatocellulært carcinom, særligt ved hæmokromatose og posthepatisk cirrose -- her evt. hyppigste årsag)
	-  Infektionssygdomme

Øget risiko for såvel venøse tromboemoliske komplikationer samt blødninger.

##### Klinisk vurdering 
ved `Child-Pugh` score (ascites, hepatisk encefalopati samt bilirubin, albumin og koagulationsfaktorer, s. 1623) eller `MELD`-score (ren biokemi).

#### Diagnoser og differentialdiagnoser
Ofte med ukarakteristiske symptomer; særligt ved højt alkoholforbrug.

Diagnosen kan ofte stilles ud fra kliniske tegn (se `Symptomer og kliniske fund`). Guldstandard er fortsat **biopsi**.

##### Differentialdiagnoser

- Cancer hepatis (primær el. sekundær)
- Kronisk hepatitis (alkoholisk, inflammatorisk eller infektiøs)
- sten- eller infektinssygdomme i galdeveje

Ultralyd kan udelukke fokale processer i abdomen; beskrive ascites og splenomegali; identificere tromber.

#### Behandling
For praktiske formål irreversibel og ubehandlelig. Behandling defor rettet mod at

- fjerne ætiologisk faktor
- identificere og behandle kompllikationer
	- herunder ernæringsterapi (bedrer korttidsoverlevelse)

I akut-i-kronisk leversvigt: leverassisterende behandling. Levertransplantation.

##### Socialmedicin
Desuden socialmedicinske aspekter (invaliderende sygdom -- der skal tages forbehold).

### Portal hypertension

#### Definition og karakteristik
- Tryk i v. portae > 7 mmHg (op til 20-25 mmHg). 
- Trykgradient over leveren er prognostisk afgørende -- gradient > 6 mmHg er abnormt.
- Ascites, øsofagogastriske varicer ved gradient > 12 mmHg.

#### Ætiologi
Præhepatisk (se `Vaskulære lidelser`)

- Portatrombose
- Isoleret miltvenetrombose

Intrahepatisk

- Præsinusoidal
   - Abnormiteter i portalrum.
   - Trykgradient *ikke* forhøjet
   - Årsager
       - Schistosomiasis (hyppigst globalt)
       - Sjældent, kræver biopsi
           - Kongenit hepatisk fibrose
           - Portasklerose
           - Myeloproliferative infiltrater
           - Sarcoidose
           - Malignt lymfom
           - Mastocytose
- Postsinusoidal (kun denne [af intrahepatiske?] forhøjet trykgradient)
    - Cirrose
        - regenerationsknuder og fibrose kompromitterer fine forgreninger af vv. hepaticae
        - forandringer i Disse's rum inkl. kollagenaflejring
        - påvirkning af stellate celler i sinusoidevæg  (via NO, endothelin) => øget portaltryk og trykgradient
    - Inflammation (især alkoholisk hepatitis)

Posthepatisk

- Trykforhøjelse i højre atrium
    - Medfører ascites, men sjældent øsofagusvaricer pga. mindre trykgradient (frit levervenetryk dog forhøjet)
- Stenosering af v. cava over lever
- Okklusion af små eller store levervener
    - Store vener: *Budd Chiaris syndrom* (hvis pga. trombose el. tumorindvækst)
    - Små vener: Veno-okklusiv sygdom (VOD) -- efter medikamentel behandling, især ved organtransplanterede (se `Vaskulære lidelser`)
 
#### Patogenese
- Blødning 
    - Kollateraler
        - Øsofagus
        - Ventrikel
        - Rectum
            - Sjældent alvorligt *undtagen ved stomier*
        - Umbilicus
        - Retroperitonealt, især i miltregionen
    - Portal hypertensiv gastropati (øgede submukøse A-V-kommunikationer i ventrikelslimhinde)
        - Akut og kronisk blødning
- Ascites
- Splenomegali
    - Mekaniske gener
    - Hæmatologiske forandringer
- Portosystemisk encefalopati (tarmblod by-passer leveren)
    - Endotoksinæmi (alm. ved cirrose), kan medføre arteriel hypotension og feber
- Nedsat first-pass-metabolisme

#### Symptomer og kliniske fund
Akut

- Akut tillukning af v. portae: Kredsløbsshock, undertiden tarminfarkt.
- Budd-Chiaris syndrom, veno-okklusiv sygdom: Over dage til uger 
    - abdominalsmerter under højre kurvatur
    - hepatomegali
    - ascites
    - evt. leverinsufficiens

Længerevarende

- Blødninger
    - øsofagusvaricer
    - portal hypertensiv gastropati
    - blødning fra duodenum og colon sv.t. portal hypertensiv gastropati
- Fund
    - caput Medusae (sjældent)
    - øget venetegning på abdomen med flow til begge vv. cavae (hyppigt)
- Trombose af v. cava inferior
    - Deklive ødemer
    - Stort kollateralt venenet i lysken
    - Blod fra abdominale vener løber til sternum 
- Splenomegali
    - Mekaniske gener
    - Hypersplenisme (pancytopeni)
    
#### Diagnose
Påvisning af

- øsofagusvaricer (gastroskopi)
- ascites (ultralyd, klinisk undersøgelse)

Støttes af

- prominerende abdominalvener
- caput Medusae
- splenomegali
- UL af v. portae
    - øget diameter
    - nedsat lineær strømningshastighed
    - retrograd flow

Trykforhold

- højt leverveneindkilingstryk of høj trykgradient <=> postsinusoidal intrahepatisk portal hypertension
- normale tryk -- præsinusoidal intrahepatisk og præhepatisk portal hypertension
    - med mindre portaltryk måles direkte v. portalvenekaterisation el. miltpunktur

### Øsofagusvaricer og gastriske varicer

#### Definition
Udvidelse af submukøse vener i nedre del af oesophagus el. ventrikel, oftest fundus. Kan opstå i andre GI-dele, fx dueodenum, colon. *Skyldes altid portal hypertension*

#### Forekomst
Hos de fleste cirrosepatienter. Hos halvdelen ved cirrosediagnosen.

Globalt set hyppigst schistosimiases, sjældnere portal- eller miltvenetrombose.

#### Symptomer og kliniske fund
Ingen symptomer i sig selv -- debuterer med blødning. Mindre blødning overses indtil blødningsanæmi; større blødning medfører hæmatemese, melæna -- evt. frisk blod per rectum.

Kan ikke skelnes fra ulcusblødning.

Ved cirrosepatienter er hæmatemese i 75% tilfælde forårsaget af øsofagu- eller fundussvaricer, resten: ulcus, portal hypertensiv gastropati, andre årsager.

#### Diagnose
Påvises ved oesofagogastroskopi. Inddeles i grad I-III afh. af prominens ifht. diameter. *Cherry-red spots* indikerer stor risiko for blødning.

#### Forløb og prognose
Akut blødning livstruende -- mortalitet ca. 15%. Samtidig kompromitteret nyrefunktion og dårlig leversyntesefunktion er markører for dårlig prognose. Risiko for reblødning 60%, dødelighed 35%.

Jo sværere grad, desto større risiko, særligt ved *Cherry-red spots*. Risiko for reblødning mindre hvis hepatisk trykgradient \< 12 mHg.

#### Differentialdiagnose
Øsofagogastroskopi -- ved aktiv blødning svært at få overblik. Alternativer er

- ulcus
- Mallory-Weiss læsion
- oesophagitis
- portal hypertensiv gastropati
	- hyperæmisk slimhinde med mosaiklignende billede ("giraftegning")
- portatrombose, miltvenetrombose: variceblødning uden cirrose
- Gastrisk anral vaskulær ectasi (GAVE)
	- stjerneformet hyperæmi i antrum; bløder sjældent voldsomt.

#### Akut behandling
##### Mål:

- Sikre blodvolumen
- Stoppe blødning
- Stabilisere funktion af vitale organer
	- Sikre blod-gas-tensioner
	- Blodtryk
	- Blodbillede: undgå
		- systemisk inflammation
		- hyperammonæmi
		- hypoxi
		- hypofosfatæmi
		- hyponatriæmi
- Reducere skadelig virkning af blod i tarmkanal (giver ændret AA-sammensætning i blod samt hyperammonæmi)

##### Medicin
Se tab. 40.5 s. 1629. Inkluderer

- blodtransfusioner (obs. overtransfundering pga. venetryk)
- terlipressin i.v. (2 mg/4 time) og/eller octreotid (50 mikrog i.v. som bolus efterfulgt af infusion af 50 mikrog/time i 48 timer)
- laktulose (til 3 afførigner dagligt)
- antibiotikumprofylakse (fx ciprofloxacin 500 mg x 2 i.v. (p.o. når muligt) i 7 dage.

##### Kirurgisk
- Endoskopisk vurdering, evt. ligering/banding hvis farmaka utilstrækkelige. Kontrollerer blødning i 70-90% af tilfælde, kræver ofte flere sessioner.
- *Sengstakensonde*: 2 balloner; en distal i cardia og en proksimal i oeosophagus. Komprimmerer vener. Standser blødning i 60-90% af tilfælde. Stor risiko for reblødning. *Bør begrænses til 24 timer pga. risiko for nekrose*.
	- Pga. risiko for aspiration og kvælning: altid fast vagt og saks på stuen.
- Stent i oesophagus (Ella-Danis stent): Teknisk mere krævende, færre bivirkninger. Kan ligge 14 dage, pt. kan være oppegående, skal dog lejres med hovedet højt (cardia holdes åben). *Velegnet som "bro til TIPS"*
- TIPS (transjugulær intrahepatisk portosystemisk shunt). *Forbinder v. portae og levervener intrahepatisk*. Reducerer trykgradient over leveren.
	- Vigtigste risiko: Udvikling af portosystemisk hepatisk encefalopati (10-15%).
	- Fordel: leverens store kar efterlades urørt -- letter evt. senere transplantation.

#### Forebyggelse
##### Primær forebyggelse
Ætiologisk behandling af cirrosen. Udvikling af varicer kan ikke forebygges medikamentelt, behandling skal forebygge blødning.

Ikke tidligere blødende varicer: Non-selektiv $\beta$-blokker (fx propranolol 40 mg/dag stigende til maks. 160 mg efter effekt) -- mål: trykreduktion på min. 20% eller til under 12 mmHg.

##### Sekundær forebyggelse
Forebyggelse af reblødning. Øsofagoskopikontrol (m. banding) eller non-selektiv $\beta$-blokker. Kombination heraf reducerer risiko for reblødning m. 50%, forlænger overlevelse.

Ved recidiverende blødninger: Vurdering for TIPS eller levertransplantation.

### Ascites
#### Definition og karakteristik
Fri væske i peritonealhulen. Ofte 5-10 liter, undertiden 25 eller derover. Elektrolytter som plasma, makromolekyler typisk i lavere koncentration.

#### Forekomst
60% af cirroseptt. debuterer med ascites. Stor andel af kompenseret cirrose udvikler ascites senere.

Typisk forløb:

1. Ascites der kan behandles med diuretika
2. Behandlingsresistent ascites
3. Hyponatriæmi og hepatorenalt syndrom

Ca. 6.000 ptt. i DK med cirrosebetinget ascites. Median overlevelse ca. 3 år.

#### Ætiologi
\* Markerer eksudater, og \** markerer inflammatoriske eksudater -- øvrige er transsudater (eller ukendt).
 
- 75% cirrose
- 15% maligne tumorer med peritoneal carcinose*
	- bl.a. ovariecancer (Meigs syndrom = ovariecancer + ascites + højresidig hydrothorax)
- 10% udtalt højresidig hjerteinsufficiens
	- konstriktiv pericardit
	- trikuspidalfejl
- Øvrige:
	- Budd-Chiaris syndrom (levervenetrombose)
	- veno-okklusiv sygdom
	- v. cava inf. ombstruktion over leverveneniveau
	- pancreatogen ascites **
	- læderede lymfekar (kyløs ascites)
	- Sjældent
		- cholaskos **
		- peritoneal tuberkulose *
		- sarcoidose
		- mastocytose
		- myelofibrose
		- schistosomiasis
		- hypothroidisme
		- bindevævssygdomme
	- I begrænset omfang:
		- portvenetrombose
		- mesenterialvenetrombose
		- inflammatoriske lidelser i lever (især alkoholisk hepatitis)

#### Patogenese
Ikke fuldt klarlagt.

Ved stor ascitesproduktion: Læk af væske fra leversinusoider til peritoneum -- dette forudsætter 1) forhøjet tryk i sinusoider (sinusoidal, postsinusoidal portal hypertension) og 2) nedsat renal udskillelse af vand og Na\textsuperscript{+}. Lav P-Albumin kan spille en mindre rolle.

Nedsat renal udskillelse skyldes mekanismen

1. Perifer vasodilatation (karakteristisk for cirrose) => lavt BT
2. Kompensatoriske meksanismer, bl.a. RAAS, ADH og sympathicus => nedsat renal perfusion.
	- RAAS (især aldosteron) hæmmer natiumudskillelsen pga. høje niveauer og øget renal følsomhed.
3. Perifer vasodilatation modvirker stigning i BT => ødemer.

Renal prostaglandinproduktion sikrer et minimum af Na\textsuperscript{+}-udskillelse -- obs. NSAIDs.

Ascites drænes via lymfen, dog maks. 700 mL/døgn (maks. effekt af diuretikabehandling).

#### Symptomer og kliniske fund
Kan udvikles akut (over dage) men er ofte snigende. Patient klager over

- stigende abdominalomfang og vægtøgning
- nedsat appetit
- åndenød ved spændt ascites eller komplicerende hydrothroax

##### Kliniske fund
Ved kraftig ascites er

- hud på abdomen blank og spændt (evt. med striae distensae og caput Medusae).
- costalbuer everterede
- lungegrænser oprykkede
- umbilicus fremstående

Evt. ledsagende deklive ødemer, evt. ødem af scrotum og penis.

##### Komplikationer
- Spontan bakteriel peritonitis
- Hernier, hyppigt umbilicale og scrotale
- Hydrothorax, hyppigt højresidigt
- Spændt ascites, der hæmmer respiration eller cirkulation
- Ruptur, ofte af tyndvægget umbilicalhernie.

#### Parakliniske fund
Diagnostisk punktur i venstre fossa iliaca. Meget værdifuld prøve, få komplikationer. *Bør udføres ved enhver indlæggelse af patient med ascites for også at opdage subklinisk spontan bakteriel peritonitis.*

Undersøgelser inkluderer

- farve (normalt klar og strågul, kan ved ukompliceret cirrose være mælket ["pseudochyløs ascites"])
- differentialtælling af
	- leukocytter
	- erytrocytter
	- protein (typisk \< 20g/L, ved transsudater højere)
		- albumin
		- amylase (pancreatogen ascites)
		- bilirubin (cholaskos)
		- triglycerid (kyløs ascites)
	- tumorceller
- dyrkning

#### Diagnose og differentialdiagnoser
##### Differentialdiagnoser
Bør overvejes:

- Adipositas
- Graviditet
- Ovariecyste
- Blæreretention

##### Diagnose
Sikreste kliniske tegn: Dekliv dæmpning i flankerne, der ændres ved lejeskift. Kan påvises klinisk ved ca. 2 liter væske. Ultralyd kan påvise mindre mængder og ved tvivl.

Fortolkning af resultater

- blodig ascites: cancer, pancreatitis
- chyløs ascites: obstruktion af lymfekar i porta hepatis (tumor, postoperativt)
- \> 250 granulocytter/$\mu$L: spontan bakteriel peritonitis
	- \> 2000 granulocytter/$\mu$L: overvej hul på hulorgan, intraabdominal absces og andre årsager til bakteriel peritonitis.
- tumorceller: falsk-negative svar hyppige
- højt amylase-indhold: pancreatogen ascites
- forskel i albuminkoncentration mellem ascites og plasma
	- \< 11g/L: taler mod cirrose og for carcinose, pancreatit og sjældent TB.
	- \> 11g/L: taler for cirrose, højresidigt hjertesvigt og nefrotisk syndrom

Ved ukendt ætiologi: ultralydsscanning, evt. laparoskopi for at afsløre peritoneal carcinose eller infektion (TB). Kvinder hvor cirrose ikke er oplagt bør gynækologisk undersøges.

#### Behandling
Beskeden ascites uden komplikationer behandles sjældent.

Primær behandling er at fremkalde negativ vand-natriumbalance (diuretika, natriumrestriktion, sengeleje), men ved terapisvigt kan medanisk tømning (laparocentese), reduktion af portaltryk (TIPS) og evt. transplantation overvejes.

Mål er desuden at undgå komplikationer som spontan bakteriel peritonit.

##### Mål: 
Da ascites kun mobiliseres med 700 ml/døgn tilstræbes vægttab på 0,5 kg/døgn hos ptt. med ascites alene og 1-1,5 kg/døgn hvis tillige perifere ødemer.

##### Regimer
- Natriumrestriktion (20 mmol/24 t)
	- Negativ natriumbalance hos 20% af ptt. dog uspiselig diæt og forværrer malnutrition.
	- *Det frarådes at spise særligt saltholdige fødevarer* (reducerer Na\textsuperscript{+}-indtag fra 150 til 90 mmol/dag.
		- Saltlakrids og vingummi frarådes -- dels pga. saltindhold og dels pga. antagonistisk effekt på spironolacton.
- Diuretika
	- Førstevalg: aldosteronantagonister (pga. aldosterons patogenetiske rolle og fordelagtig K\textsuperscript{+}-besparelse)
		- Spironolacton 100-400 mg/dag
		- \> 50% responderer på natriumrestriktion + spironolacton.
		- Bivirkninger
			- gynækomasti
			- hypovolæmi med aftagende nyrefunktion og elektrolytforstyrrelser
			- dehydrering med hepatisk encefalopati
	- Supplering med loopdiuretika, fx furosemid op til 160 mg/dag
	- Opbygning
		1. let saltrestriktion, 100 mg spironolacton $\times$ 1
		2. 200 mg spironolacton + 40 mg furosemid
		3. 300 mg spironolacton + 80 mg furosemid
		4. 400 mg spironolacton + 80 mg furosemid $\times$ 2
	- Dosering justeres hver 3.-5. dag efter respons og P-Kalium. Døgn-urin Na\textsuperscript{+} kan være nødvendig (høj udskillelse skal medføre saltrestriktion)
	- Behandling påvirkes negativt af NSAID og ACE-hæmmere (nedsætter GFR)
	- Alkoholisk cirrose: abstinens vil ofte medføre forbedret leverfunktion, så diuretika kan reduceres/seponeres efter måneder
- Sengeleje
	- *Anbefaling er hvile midt på dagen* pga. negative effekter ved immobilisering.
	- Øger renal gennemblødning og GFR med 30%
	- Fordobler natriuretisk effekt af loopdiuretika.
- Væskerestriktion
	- Kan teoretisk forværre funktionel hypovolæmi.
	- Ved normal P-Natrium ingen effekt. Ved fortyndingshyponatriæmi kan pt. tørstes, dog svært at udføre. Ved meget stort væskeindtag kan dette begrænses til 2-2,5 L/dag.
		- Fortyndingshyponatræmi
			- Skyldes nedsat frit-vands clearance pga. højt ADH og samtidig natriuretiske diuretika => ubalanceret diurese.
			- Giver sjældent neurologiske symptomer pga. langsom udvikling. Meget lav natrium er dog risikofaktor for hepatisk encefalopati.
			- *Anbefaling er reaktion v. P-Natrium \< 130 mmol/L* ved seponering af diuretika som det første. Afhængig af væskevolumen enten isotont NaCl (hypovolæmisk) eller væskerestriktion (hypervolæmisk).
			- ADH-antagoniser har begrænset anvendelse pga. bivirkninger.

##### Refraktær og resistent ascites
*Definition af diuretikaresistent*: Ascites, der ikke mobiliseres på

- diæt med 90 mEq Na\textsuperscript{+} dagligt
- spironolacton 400 mg dagligt
- furosemid 80 mg $\times$ 2 dagligt

*Definition af diuretikarefraktær*: Ascites, der kun mobiliseres på bekostning af diuretika-inducerede komplikationer (som tiltagende nedsat nyrefunktion eller encefalopati). Ses i 10-20% af tilfælde.

##### Terapeutisk laparocentese:
Udtømmelse eliminerer mekaniske gener.

Indikationer:

- spændt ascites
- diuretikaresistent og -refraktær ascites
- behov for tømning aht. diagnostiske procedurer

Dræn må højest ligge 24 timer for at undgå infektioner.

Der gives albumin ved procedurestart for at undgå central hypovolæmi og yderligere nedsat nyrefunktion.

Behandling bør følges op med diuretika da ascites hurtigt gendannes.

##### TIPS
(transvenøs intrahepatisk portosystemisk shunt). Aflaster tryk i sinusoider og resten af splanchnicusgebetet, fjerner årsag til ascitesdannelse.

Meget effektivt, bedrer overlevelse ved refraktær og resistent ascites.

Risiko er udvikling eller forværring af hepatisk encefalopati.

Overvejes ved behov for laparocentese mere end én gang månedligt hos patienter uden tendens til encefalopati.

##### Levertransplantation
Ultimativ behandling -- ascites sjældent eneste indikation.

### Spontan bakteriel peritonitis
- Hyppigste infektion hos patienter med cirrose og ascites. Forekommet hos 5% af ptt. uden symptomer og hos 25% af hospitalsindlagte.
- Skyldes formentlig oftest translokation af bakterier fra GI-kanal.
- Positive dyrkningssvar i 1/3 af tilfælde, hyppigst gramnegative -- oftest *E. coli*

#### Klinisk mistanke
Opstår ved

- feber
- abdominalsmerter
- leukocytose
- uforklaret sepsis
- encefalopati

En del patienter få/ingen symptomer -- derfor terapeutisk ascitespunktur v. alle indlagte.

#### Diagnose og behandling
Diagnose stilles ved granulocyttal i ascitesvæske \> 250 $\mu$g/L.

Behandling indledes ved diagnose -- der ventes ikke på dyrkningssvar. Behandling starter med i.v. cefotaxim 2x $\times$ 2 dagligt i 5-7 døgn.

Hos ptt. med nyrepåvirkning eller bilirubin \> 68 $\mu$M gives i.v. albumin 1,5 g/kg på førstedag samt 1 g/kg på tredjedagen for at reducere incidens af hepatorenalt syndrom og øge overlevelsen.

#### Profylakse
Recidiverende peritonitis ved fortsat ascites: ciprofloxacin p.o. 750 mg $\times$ 1 ugentlig.

Cirrose og ascites (\< 15g protein pr. liter) uden tidligere peritonit: 500 mg ciprofloxacin $\times$ 1 (daglig?). Risiko er udvikling af ciprofloxacin-resistente bakterier.

### Hepatorenalt syndrom (HRS)

#### Definition og karakteristik
Potentielt reversibel nyrefunktionsnedsættelse, som optræder ved svær leversygdom (hyppigst cirrose men også akut fulminant leversvigt og alkoholisk hepatitis).

Karakteriseret ved renal hypoperfusion uden morfologiske forandringer i nyrerne.

Pragmatisk set en eksklusionsdiagnose, som hos kendte leversyge patienter kræver udelukkelse af andre årsager til nyresvigt

- shock
- hypovolæmi
- medikamentel nefropati
- parenkymatøs nyresygdom
- urinvejsobstruktion

Forekommer i to former

- Type 1 HRS: Hurtigt aftagende nyrefunktion -- fordobling af P-Kreatninin eller stigning til mindst 221 $\mu$mol/L på \< 2 uger
- Type 2 HRS: Langsommere udvikling, P-kreatinin i interval 130-220 $\mu$mol/L. Hyppigst hos patienter emd refraktær ascites og udtalt Na\textsuperscript{+}-retention.

#### Forekomst
Hyppigst v. svært nedsat leverfunktion (resistent/refraktær ascites). Ved hepatisk encefalopati vil ca. halvdelen have nyrefunktionsnedsættelse.

Ved type 1 HRS er der udløsende faktor: sepsis, svær alkoholisk hepatitis, stort volumentab ved laparocentese

#### Ætiologi
Nedsat leverfunktion med derangeret kredsløb, karakteriseret af udtalt perifer vasodilatation. Derudover ukendt.

#### Patogenese
Slutstadiet af cirrosens udvikling, opfattes som sekundært til den perifere arterielle vasodilatation.

Kan forstås som en frustran overkompensation for nedsat fyldning i central del af arteriesystemet med nedsat nyreperfusion til følge. Omfatter de fleste vasoaktive akser, vigtigst er

- RAAS
- sympatichus
- ADH

Ingen morfologiske ændringer i nyrer: nyrefunktion genoprettes ved levertransplantation, og nyrer kan benyttes som donornyrer.

#### Symptomer
*Leversvigt*. Sjældent klassiske symptomer på uræmi, men dog: træthed, tørst, kvalme, anorexi

#### Kliniske og parakliniske fund
Ligner prærenal uræmi.

- Tidligst
	- Nedsat evne til at udskille frit vand og natrium
	- Hyponatriæmi
- Senere
	- Fald i diureser
	- P-kreatinin stiger
	- Urinosmolalitet og U-Kreatinin er høj
	- Døgnurin Na\textsuperscript{+} oftest \< 10 mmol/L
	- Urinmikroskopi normal

##### Diagnosen 
kræver kreatininforhøjelse uden bedring efter 2 dages diuretikapause under volumenekspansion med 100 g albumin/dag samt udelukkelse af shock, nefrotoksiske lægemidler og parenkymatøs nyresygdom.

Ofte dog klinisk oplagt og kan stilles umiddelbart.

#### Prognose
Dårlig prognose. Type 1 HRS: medianoverlevelse ca. 2 uger; ved type 2 HRS: 6 mdr.

Kun bedring af leverfunktion forbedrer prognose. Kan bidrage til indikation for levertransplantation trods noget ringere overlevelse end ved manglende HRS.

#### Diagnose og differentialdiagnoser
Ligner paraklinisk prærenal uræmi ved

- normal urinmikroskopi
- U-natrium \< 10 mmol/L

I modsætning prærenal uræmi ses *ikke bedring ved simpel volumenekspansion* (se desuden under `Kliniske og parakliniske fund`).

*Akut tubulointerstitiel nefropati* (ATIN): Også udløst af hypotensiv episode -- her

- isoosmotisk urin
- U-Natrium på 40-50 mmol/L

#### Behandling
Patienter med type 1 HRS indlægges

- Plasmavolumen øges med 200 ml 20% human albumin dagligt
- Terlipressin 1 mg $\times$ 4 stigende til maks. 2 mg $\times$ 6 fortsættes til P-kreatinin \< 130 $\mu$mol/L -- dosis øges med 1-2 dages mellemrum
	- Medfører mesenteriel arteriel vasokonstriktion og reduktion i portaltryk samt øger nyreperfusion.
	- Bivirkning
		- Alvorlig tarmiskæmi og hypoxi i ekstremiteter
		- Hvis ikke bedring v. volumenekspansion + nitroglycerin reduceres dosis
- Diuretika seponeres (kan forværre tilstand)

TIPS bedrer nyrefunktion og overlevelse. Konventionel dialyse ændrer ikke prognose, men kan anvendes mens ætiologi behandles, eller der ventes på levertransplantation.

### Kardiale og pulmonale komplikationer til portal hypertension

#### Cirrotisk kardiomyopati
Systolisk dysfunktion med nedsat kontraktil kraft ved arbejde, kombineret med diastolisk dysfunktion og forlænget Q-T-interval.

Ved arbejde øges venstre ventrikels EF mindre end hos kontroller. Sygdom forskellig fra alkoholisk kardiomyopati.

Kan medvirke til udvikling af hepatorenalt syndrom og hjertesvigt efter invasive procedurer.

Cirrosepatienter har forhøjet MV -- faldende MV er forbundet med dårlig prognose.

Ætiologi og prævalens ukendt.

#### Hepatopulmonalt syndrom (HPS)
##### Definition
Tilstedeværelse af alle følgende

- portal hypertension
- arteriel hypoxi med P\textsubscript{a}O\textsubscript{2} \< 70 mmHg
- pulmonal-arterie P\textsubscript{a}O\textsubscript{2} \> 15 mmHg 
- intrapulmonal vasodilatation.

Tilstand er reversibel, fx efter levertransplantation. 

##### Klinisk karakterisation 
- funktionsdyspnø, efterhånden hviledyspnø og orthodeoxi (dyspnø i stående men ikke liggende stilling)
- hurtigt faldende iltsaturation ved arbejde
- ilttilskud har begrænset effekt
- arteriel iltmætning er lavere i stående end liggende stilling

##### Diagnose og prognose
Kan stilles ved kontrast-ekkokardiografi (hurtig kontrastpassage fra højre til venstre ventrikel bekræfter eksistens af pulmonale shunts).

Forværrer overlevelsen. Hyppighed er ca. 10% af cirrosepatienter.

#### Porto-pulmonal hypertension
Sammenfald mellem portal hypertension og pulmonal arteriel hypertension. 

##### Definition
- Middel pulmonalt arterietryk \> 25 mmHg
- forhøjet pulmonal vaskulær modstand (\> 240 dyn $\times$ sek $\times$ cm-5 [?!])
- middel pulmonalt okklusionstryk \< 15 mmHg

Ses ved cirrose og portal hypertension i 2% af tilfælde.

##### Symptomer
er træthed og dyspnø, typisk progredierende.

##### Prognose
Middeloverlevelse ca. 6 mdr.

##### Behandling
Kan ikke behandles med TIPS eller levertransplantation (øger pulmonalt tryk yderligere). Lungesymptomer behandles som primær pulmonal hypertension.

### Hepatisk encefalopati
#### Definition, karakteristik, klassifikation
Påvirket hjernefunktion (neuropsykiatrisk), oftest med *nedsat bevidstheadsniveau* hos patient med leversygdom, hvor anden årsag ikke er påvist.

Metabolisk syndrom -- kan være *akut* (ofte episodisk, evt. recidiverende [hyppigst ved cirrose]; pga. tab af leverfunktion) eller *kronisk* (også pga. portosystemisk shunt eller leversygdom).

Klassificeres som *overt* (manifest, klinisk erkendelig) eller *minimal* (diskrete personlighedsændringer, påvirkede neuropsykologiske tests).

#### Forekomst, epidemiologi
Optræder hos 75% af cirrosepatienter, oftest flere gange. Halvdelen af tilfælde overte.

5% af cirrosepatienter debuterer med encefalopati.

Kronisk form er sjælden.

*Akut (fulminant) leversvigt er defineret ved optræden af hepatisk encefalopati.*

#### Ætiologi
Kræver funktionelt leversvigt. I 3/4 af tilfælde findes én eller flere udløsende faktorer.

#### Patogenese
Forhøjet plasma-ammoniak. Ammoniak er '*first-hit*': optages i CNS og har metabliske virkninger i neuroner. Afgiftes ved indbygning i glutamin -- medfører cellulært ødem og nedsat glutamaterg neurotransmission. Forstyrrer energistofskiftet.

Kræver også et '*second-hit*':

- Systemisk og lokal inflammation: ændring i blod-hjerne-barriere
- ændret plasma-aminosyre-sammensætning: ændet, ubalanceret optag og neurotransmitterdannelse -- ofte pga. fordøfelse af blod ved øvre GI-blødning
- hyponatriæmi
- ændringer i ilttryk, pH og temperatur

Klinisk kan findes *udløsende faktorer*:

- infektion
- øvre GI-blødning
- dyshydrering med elektrolytforstyrrelser
- hypoxæmi
- psykoaktive farmaka (morfika, benzodiazepiner, barbiturater)
- obstipation
- kirurgi

*Ikke relateret til stort proteinindtag eller glukosemangel.*

#### Patologisk anatomi
Cellulært ødem i astrocytter, særligt i cortex og basalganglier. Forandringer antages at være reversible.

#### Symptomer og kliniske fund
- Minimal encefalopati
	- Manifestation af cirrose med mest forringet livskvalitet
	- Diskrete personlighedsændringer
	- Kognitive forstyrrelser
	- Træthed
	- Søvnløshed
	- Uro
	- Fummelfingrethed
	- Psykometrisk undersøgelse: Langsommere adfærd, øget reaktionstid, specielt meget ustabile reaktionstider
- Overt encefalopati
	- Lette tilfælde (grad I)
		- Diskrete adfærdsændringer
		- Ændret vågen/søvn døgnrytme
		- Forlænget latenstid ('slow cerebration')
	- Sværere tilfælde (grad II)
		- Konfusion
	- Grad III
		- Pt. svær at vække
	- Grad IV
		- Coma

Dybde af encefalopati kan bedømmes med Glasgow Coma Scola.

Desuden ses

- dyskoordination (bl.a. dyspraksi med ændret håndskrift)
- spatial dysorientering
- hos 20% lavfrekvent rytrmisk tab af tonus på ekstensorside med frekvens på ca. 3/sek (*asterixis*) -- mest synligt med udstrakte, let ekstenderede hænder (*flapping*, *basketremor*)
- evt. universel hypertoni eller hypotoni til atoni.
- Reflekser
	- Tidligt: øgede senereflekser
	- Senere: svækkede reflekser til areflexi
	- Patologiske reflekser (Babinski) med eller uden sideforskel
	- *Normale hjernestammereflekser*
- Øjne
	- Øjenakser kan være forskellige i begge planer: svømende, dissocierede øjenbevægelser
- Langt de fleste tilfælde: *cirrosestigmata* og ofte *foetor hepaticus ex ore*

#### Parakliniske fund
- Reaktionstider: svingende, forsinket motorisk reaktion med høj spredning
- P-Ammoniak: Næsten altid mindst fordoblet.
- Koagulationsfaktorer II, VII, X: Næsten altid nedsat til det halve eller mere.
- EEG (uspecifik, bruges sjældent): langsom dominant frekvens, blandet spir- og kuppelformede komplekser
- Billedscanning: Næsten altid upåfaldende
- Aminotransferaser, basisk fosfatase: Kan ikke relateres

Udløsende faktorer påvises ved

- blod i ventrikelaspirat
- hyponatriæmi
- tegn på hypovolæmi pga. blødninng eller diaré

Celletal i ascitesvæske kan være øget pga. peritonitis. Bloddyrkning ofte positiv pga. nedsat immunforsvar og øget translokation fra tarme.

#### Forløb og prognose
Minimal encefalopati oftest langvarig, kan behandles som beskrevet nedenfor.

Overtencefalopati i akut form: varer få dage, sjældent over 1 uge. I enkelte tilfælde (oftest med store porto-systemiske shunts) er forløb intermitterende eller kronisk.

*Er cirrosekomplikationen der mest udtalt markerer dårlig prognose*. Efter sanering af udløsende årsager er dødelighed i de enkelte episoder \< 25%. *Dødsårsag hyppigst infektion (UVI, pneumoni, pakteriel peritonit) eller blødning (2/3 udvikler blødning; oftest gastrit, portal hypertensiv gastropati)*.

#### Diagnose og differentialdiagnoser
Diagnose mistænkes ved cirrosepatient, der klager dårlig livskvalitet og initiativløshed, hvis der findes

- kognitive forstyrrelser *og*
- nedsat bevidsthedsniveau *eller*
- forsinket reaktion på tiltale

Parakliniske hints (se desuden `Parakliniske fund`)

- Koagulationsfaktorer II, VII, X næsten altid under halvdelen af normalen. 
- P-ammoniak forhøjet

##### Differentialdiagnoser
- Organisk delir (dette uden nedsat psykomotorisk tempo)
- Intrakraniel blødning (CT-scan ved første episode el. ved hjernestammepåvirkning)
- Neuroinfektioner (feber, stivhed mest udtalt i nakken -- begrunder lumbalpunktur)
- Wernickes encefalopati (nystagmus, oftalmoplegi)
- Sedativa og alkohol forværrer tilstand (mål blodindhold heraf)
- Metaboliske encefalopatier (uden nedsatte koagulationsfaktorer)
	- hypoglykæmi
	- hyperosmolaritet
	- elektrolytderangement
	- anoxi
	- uræmi
	- ketoacidose
	- endokrinopatier
- Epileptiske manifestationer i form af postiktal bevidsthedsplumring, non-konvulsiv status

#### Behandling
Består af 

- Identifikation og behandling af udløsende faktorer
- Overvågning og stabilisering af patient
- Forebyggelse af yderligere kompllikationer
- Behandling af encefalopati

For egentlig behandling se Kompendiet ss. 1638-1639.

##### Behandling af encefalopati
- Ernæringsterapi
	- tilstrækkelig energitilførsel
	- hurtigt stiende proteintilskud
- Specifik behandling
	1. laktulose (forebyggende for obstipation og elimination af ammoniumioner)
	2. Forebyggelse
		- Rifaximin (tungtopløseligt antibiotikum)
	3. Eksperimentielt (se Kompendiet).

### Hepatisk malnutrition

#### Definition, karakteristik, klassifikation
Forringet ernæringstilstand hos patient med kronisk leversygdom. Oftest tab af både muskel- og fedtmasse. Protein- og kalorieunderernæring.

#### Forekomst, epidemiologi
3/4 af patienter, der indlægges, er behandlingskrævende underernærede.

#### Ætiologi
Sekundær til kronisk leversygdom med metablisk derangement. Optræder uafhængigt af leversygdommens ætiologi, samtidig neuropati og graden af inaktivitet.

#### Patogenese
Anorexi er fremtrædende, særligt ved samtidig ascites. Ofte malabsorption i forskellig art og grad, bl.a. pga.

- semi-faste med mucosa-atrofi
- fedtmalabsorption pga. cholestase
- alkoholisk enteropati

Portalhypertension, intestinal stase og enterocytdysfunktion spiller en rolle.

Proteinbehov for at opnå positiv nitrogenbalance er højere end normalt

- aminosyrestumulation af proteinsyntese er nedsat og mindre følsom for insulin
- vækstfaktorer produceret overvejende i lever (fx IGF-1) mangler
- insulinresistens kompromitterer glykogensyntese, så der lettere gås i faste-metabolisme
- nitrogenbesparende effekt af kulhydrat på proteinomsætning ophævet pga. kronisk hyperglukagonæmi
- energiomsætning er øget i tilfælde med hyperdynamisk kredsløb og ved sepsis

I enkelte tilfælde skyldes det fejlagtig rådgivning om 'skånekost'.

#### Patologisk anatomi
Ændret legemssammensætning: Nedsat muskelmasse, øget ekstracellulært vand; senere tab af subkutant fedtvæv.

#### Symptomer og kliniske fund
Patienter

- er trætte 
- er uoplagte
- er muskelsvage
- mangler madlyst
- har svært nedsatte fysiske færdigheder
- nedsat kraft og udholdenhed over næsten alle led mest proksimalt
- kan være normalvægtige pga. vækseophobning
	- afklædt patient: dårligt muskelpolster om især bækken og skuldre

#### Parakliniske fund
*Er ikke afgørende for diagnosen.* 

Mest interessant:

- Døgnkreatininudskillelse nedsat (pga. lav muskelmasse)
- Midtoverarmsmuskel-areal nedsat
- Struktureret kostinterview viser reduceret spontan indtagelse af energi og protein

#### Forløb og prognose
Malnutrition er progredierende og forbundet med forringet prognose. Korrigeres med ernæringsterapi -- næstmest effektive behandling (efter levertransplantation).

#### Diagnose og differentialdiagnoser
##### Diagnose
stilles hos den kendte kronisk leversyge patient ved iagttagelse af afklædt patient, hvor noteres

- dårlig muskelfylde
- forskellige grader af væskeophobning
- hængende hud og fedtdepoter

Biologiske mål (fx døgnkreatininudskillelse:legemshøjde-ratio) ikke mere effektive end klinisk observation.

##### Differentialdiagnoser
inkluderer underernæring af anden grund

- kræft
- kardiomyopati
- pulmonal kakeksi pga. KOL

Diabetes skal diagnosticeres.

#### Behandling
*Alle leverpatienterskal ernæringsscreenes.* Hos patienter med for ringe indtag, klinisk underernæring eller komplikationer til leversygdom er der indikation for ernæringsterapi.

##### Terapi
- energi + protein + vitaminer og mineraler
- mange små måltider
- mindst mulig faste
- overhold evt. saltrestriktion i ascitespatienter
- mobilisering og styrketræning

Kan være svært at indtage al føden i starten; ernæringsdrikke kan hjælpe. Hvis pt. ikke kan gennemføre, anlægges ventrikelsonde.

#### Socialmedicinske problemer
Ernæringsterapi fortsætter min. 3 mdr. efter udskrivelse. Hvis spinkelt socialt netværk findes ordninger til madudbringning eller kontakt til spisesteder.

Recept på proteinrigt kosttilskud som købes med tilskud.

