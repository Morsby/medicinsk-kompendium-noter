# Reumatologi

## Reumatoid artrit

### Definition og klassifikation
Kronisk autoimmun sygdom karakteriseret ved perifer og symmetrisk ledinflammation samt ekstraartikulære og systemiske manifestationer. Over tid progredierende leddestruktion, nedsat livskvalitet og øget mortalitet.

Klassificeres og diagnosticeres på basis af kriterier fra American College of Rheumatology (ACR) og European League Against Rheumatism (EULAR), 2010 (s. 373). Overordnet taler følgende for sygdom: Flere involverede små led, serologi (RF, anti-CCP), akutfasereaktanter, varighed min. 6 uger.

### Forekomst og epidemiologi
- Prævalens 0,5-1% på verdensplan. 
- Årlig incidens i DK: 36 pr. 100.000 hos kvinder, 14 pr. 100.000 hos mænd
- Debut hyppigst mellem 30-50 år; medianalder ca. 60 år, 50% før 50-års alderen.

### Ætiologi 
Ætiologi ukendt. Miljøfaktorer og genetik spiller ind. Mulige miljøfaktorer er

- infektion med bakterier/virus
	- endogen retrovirus, parvovirus, EBV, mykobakterier, mykoplasma foreslået 
- rygning
- endokrine faktorer (hypothalamus-hypofyse-binyre-aksen; østrogen)
- m.fl.

Mulige genetiske faktorer er

- Vævstyper
	- HLA-DR4 og DR1 (*the shared epitope*)
	- HLA-DRB1 alleller associeret til særligt erosivt forløb
-  Anden genetisk disposition
	- MBL muligvis involveret i den kroniske inflammation 
	- Polymorfi af immunologiske mediatorer (cytokiner, TCR, immunglobuliner) uden endelig afklaring

### Patogenese
Involverer både kroniske cellulære og humorale responser, inkl. memoryceller.

- Reumafaktor (IgM-, IgA- og IgG-antistoffer mod Fc-del af IgG) findes hos de fleste: 35% ved debut, ca. 70% i løbet af sygdom
- Anti-CCP (mod citrullinholdige cykliske peptider): samme sensitivitet men højere specificitet. Forekommer tidligt i forløbet.

#### Cytokiner og kemokiner
- Makrofagderiverede. Især: IL-1, IL-6 og TNF-$\alpha$.
	- TNF-$\alpha$ stimulerer andre cytokiner og bevirker vævsnedbrydning i led via kollagenase-dannelse, osteoklastaktivering og kemotaksi af leukocytter
- Proinflammatoriske arakidonsyremetabolitter (særligt PGE2) er centrale og stimuleres af bl.a. IL-1 og TNF-$\alpha$. 
- ROS spiller også en rolle i vævsskaden via hypoxi.
- Neuropeptider fra nociceptive terminaler: Ved inflammation frigives substans-P og calcitonin gen-relateret peptid (CGRP)
	- Substans-P
		- Vasodilaterende, øger plasmaekssudation, endothelcelleadhæsion af granulocytter samt aktivering af disse. 
		- Øger histaminfrigørelse fra mastceller
		- Cytokinsytense, proliferation og produktion af PGE2 samt kollagenase i synoviocytter

### Patologi
*Kronisk synovitis i ledkapsler, seneskeder og bursae er centralt.* Inflammation anses som hovedårsag til vævsdestruktion. Histologi er ikke specifik for RA.

Reumatoid inflammation: lokaliseret til synovialismembran men også seneskeder, bursae, interne organer og blodkarvægge.

- Reumatoid synovitis er præget af
	- hyperplasi og hypertrofi med villøs fortykkelse af synovialmembran
	- pannusdannelse med indvækst i brusk og knogler
	- histologisk dominans af T- og B-lymfocytter, monocytter/makrofager, enkelte mastceller -- lokalt prolifererende fibroblaster og synoviocytter samt endotelcelleprliferation med angiogenese.

#### Angiogenese og invasiv vækst
Karakteristisk fund er tumorlignende vækst af synovial pannusfront. Støttes af angiogenese -- stimuleret af både makrofager og synoviale fibroblaster.

- Involverer proliferation og invasiv vækst af synoviale fibroblaster i brusk og knogle
- Ændring af apoptoseregulerende gener spiller rolle (fx p53)
- Fremmes af PDGF, FGF, IL-1 og TNF-$\alpha$.

#### Synovitis og pannus
*Pannus* betegner den invasive front af nydannet væv.

Tidligste forandringer ses i synovialiskar

- Defekt endotelcellekontakt, evt. okkluderet lumen
- Subintima: ødem, fibrinaflejringer
- Celleinfiltration: ses tidligt, især CD4+ T-celler men også monocytter og makrofager. Plasmaceller først sent.
- Hypertrofi af synovialis pga. karnydannelse, infiltration af celler, proliferation af synoviocytter med villusdannelse, ødem, granulationsvæv og fibrose
- Nydannet væv kan invadere omkringliggende væv: brusk, ligamenter, sener, knogle
- Neutrofile granulocytter: fåtallige i membranen, *hovedparten i synovialisvæsken*

#### Brusk- og knogledestruktion
Matrixnedbrydende enzymer findes i høj konc. i både synovialismembran og -væske. Stimuleres af cytokiner, herunder IL-1 og TNF-$\alpha$, enzymerne produceres af: fibroblaster, makrofager, kondrocytter, osteoklaster og neutrofile. Osteoklaster ansvarlig for knoglenedbrydning.

#### Ekstraartikulære manifestationer
- *Noduli rheumatici* har tre distinke lag
	1.  Centralt: fibrinoid nekrose
	2. palisadeformet lag af aflange makrofager ("epiteloide celler")
	3. granulationsvæv med inflammatoriske celler og fibrose
- *Tenosynovitis* samt *pleuritis* og *pericarditis*
	- Uspecifik inflammatorisk proces, men noduli og fibrinoide nekroser forekommer
- *Vasculitis* 
	- Oftest leukocytoklastisk eller andarteritis obliterans
	- Vasculit i middelstore og større arterier forekommer, men nekrotiserende vaskulit er sjælden
- *Diffus interstitiel lungefibrose*
	- Kan histologisk ikke adskilles fra andre typer lungefibrose
- *Sjögrens syndrom*
	- Kan histologisk ikke skelnes fra primær Sjögrens
- Amyloidose
	- Påvises hos op til 25% ved autopsi; giver sjældent symptomer -- hyppigst proteinuri

### Symptomer og kliniske fund
- Hos 2/3: Snigende symptomudvikling over uger til måneder
- Hos 1/3: Akut eller subakut symptomudvikling over dage til få uger

Symptomer inkluderer

- almen sygdomsfølelse (træthed, subfebrilia, uspecifikke muskuloskeletale smerter) -- obs. fejldiagnosticiering: polymyalgia rheumatica
- ledsymptomer
	- alle led kan involveres
	- *hyppigst MCP, PIP og håndled samt metatarsofalangealledene*
	- ofte symmetriske
	- hos omkring 80% polyartikulære ved debut
- muskelatrofi og -svaghed udvikles hurtigt
  
#### Artikulære og periartikulære manifestationer
I tidlige faser betinget af inflammation i kapsel, seneskeder og bursae. I forløbet indtræder *progredierende fejlstillinger og funktionsnedsættelse pga. irreversible led- og seneforandringer, og der udvikles muskelatrofi og osteoporose.*

Regionale forandringer afhænger af intensitet og varighed af inflammation samt af fysisk belastning.

##### Ledsymptomer
Omfatter

- stivhed
	- mest udtalt efter *inaktivitet*, særligt karakteristisk morgenstivhed
- smerter = artralgier
	- primært funktionsrelaterede -- men ved udtalt synovit også i hvile
	- i tidlige stadier: pga. inflammation og væskeansamling. Led holdes ofte let flekteret for at øge kapselrum og reducere spænding
- funktionsnedsættelse
	- pga. smerter og 
	- tidligt: muskelatrofi
	- senere: irrversible ledforandringer:
		- brusk- og knogledestruktion
		- ossøs ankylose
		- kapsel fibrose
		- kontraktur

###### Kliniske fund 
er

- hævelse
- ømhed
- varme over leddet
- tegn på ansamling i leddet
- bevægeindskrænkning
- *rødme er sjælden, tyder på infektion* -- hud over fingerled kan dog være ret rødlig/blålig

###### Ledaffektionens topografi
- Symmetrisk polyartrit hyppigst
- Hyppigste led:
	- Fingrenes grund- og mellemled
	- håndled
	- tæernes grundled
- Inddragelse af større led også almindeligt

Undertiden oligoartikulær debut, sjældere monoartikulær.

*Hofteled og distale interfalangealled på hænder og fødder inddrages sjældent*.

- Kæbeled og cervikalcolumna involveret hos 1/3
- Columna thoracolumbalis og sacroiliacaled som regel ikke.

##### Tenosynovitis og bursitis
###### Tenosynovitis
- hyppig, særligt i
	- hænder og håndled
	- fødder og fodled
- viser sig ved
	- hævelse
	- ømhed
	- funktionsrelaterede smerter
	- krepitation
- længerevarende tenosynovit medfører risiko for indvækst af granulationsvæv og *seneruptur*

###### Bursitis
- Hyppigt med lednær bursitis
- Viser sig ved lokaliseret hævelse og ømhed med fluktuation som følge af ekssudation i bursae 
- Snævre pladsforhold kan medføre tryksymptomer: *karpal- og tarsaltunnelsyndromer*

##### Muskelatrofi
Konstant fund. Indtræder hurtigt (uger), kan være udtalt. *Typisk i muskler, der bevæger sig over involverede led.* 

Inaktivitet antages at være væsentligst, men evt. også neurogen årsag. Histologisk undertiden let interstitiel myosit (uden klinisk betydning). 

Muskelenzymer i plasma normale.

##### Osteoporose
Regionalt og systemisk. Systemisk skyldes

- systemisk inflammation
- nedsat fysisk aktivitet
- evt. systemisk glukokortikoidbehandling

##### Karakteristiske regionale forandringer i bevægeapparat 
###### Hænder og håndled 
Symmetrisk synovit i håndled, MCP- og PIP-led er RA-kendetegn. Inddragelse af PIP er sjælden.

Ved vedvarende synovit udvikles fejlstillinger:

- Håndled
	- let radial deviation
	- undertiden let volar subluksation
	- dorsal subluksation af caput ulnae
- Hænder
	- 2.-5. fingers grundled (især 5., sv.t. håndled)
		- ulnar deviation
		- let volar subluksation
	- *Svanehalsdeformitet* består af
		- hyperekstension i PIP-led
		- fleksionskontraktur i yderled
	- *Knaphulsdeformitet* består af
		- fleksionskontraktur i PIP, som er lukseret ud gennem eksentorsenernes to crura
		- hyperekstension af DIP
	- *Z-deformitet* i tommelfingre består af
		- fleksion i grundled
		- hyperekstension i yderled

Tenosyovitter:

- Inddrager dorsale seneskeder på håndled og fleksorsenerne på fingrenes volarside (evt. hævelse, ømhed)
- *Springfinger*
	-  Stenoserende tenosynovit i fleksorsenernes seneskeder
	-  Lokaliseret øm fortykkelse, undertiden krepitation
- I dorsale håndledssener: evt. ruptur medførende *dropfinger*.
- *de Quervains syndrom*
	- Tenosynovit af tommels ekstensor- og abduktorsener

Bursitis:

- I bursa manus (profund for lig. carpi transversum) kan medføre kompression af n. medianus og *karpaltunnelsyndrom*.
	- paræstesier og sensibilitetsforstyrrelser i 1.-3. samt radiale halvdel af 4. finger
	- smerteudstråling til hånd og underarm
	- atrofi og parese af themarmuskulatur
- Sjældent kompression af n. ulnaris (sv.t. karpaltunnelsyndrom blot øvrig del af hånd og fingre)

Muskelatrofi:

- Inddrager interosserne
- Tydeligst på dorsum manus
- Senere også antebrachiummuskulatur

Funktionsnedsættelse:

- Betinget af
	- Smerter
	- Bevægeindskrænkning
	- Instabilitet
	- Fejlstillinger
	- Kraftnedsættelse
- *Z-deformitet* er særligt invaliderende (kompromitterer skrivefunktion)
- *Gribestyrke benyttes som mål for resultat af forandringer*
- Andre mål er:
	- Knytdiastasen (afstand ml. fingerpulpae og vola manus)
	- Nøglegreb (afstand ml. tommelfingerpulpa og 2. fingers radialside)
	- Pincetgreb (afstand ml. tommelfingerpulpa og fingerpulpae på 2.-5. finger)

###### Albueled og skulderled
rammes hyppigt (synovit og lednær bursit). 

Albueled: 

- Ofte indtræder tidligt og ubemærket *ekstensionsdefekt* i albueled (kan oftest kompenseres af hånd- og skulderled). 
- Evt. tryk på n. ulnaris med smerter, sensibilitetsforstyrrelser, pareser

Skulderled:

- Smerter medfører *rotater-cuff insufficiens* sekundært til atrofi
	- resulterer i: 
		- instabilitet
		- subluksation
		- bevægeindskrænkning (primært af abduktion og udadrotation)
- Ansamling i bursa subacromialis kan illudere intraartikulære ansamlinger i leddet.

###### Knæled
Synovit med væskeansamling erkendes let ved prominins af bursa suprapatellaris og *anslag af patella (eller lignende)*.

- Ansamlinger kan medføre ekstensionsdefekt.
- Hurtigt udvikles quadricepsatrofi
	- Sammen med svækkelse af kollaterale ligamenter -- medførende instabilitet
- *Bakercyste*:
	- Intraartikulær ansamling og trykforøgelse kan medføre cysteagte udposning af synovialiskapslen på *bagsiden af knæet*.
	- Kan dissekere ned på crus, evt. helt til hælregionen (oftest mellem gastrocnemius og soleus)
	- I tilfælde af ventildannelse: Risiko for ruptur og *akutte symptomer i for maf pseudoflebotrombosesyndrom*: hævelse, omfangsforøgelse, øget venetegning og ømhed af crus
		- kan også ses uden ruptur ved kompression af v. poplitea
	- Kan erkendes ved UL eller MR-scanning

###### Ankler og fødder
- Artrit i talocruralled og subtale led: medfører udtalte, belastningsrelaterede smerter og evt. fejlstilstillinger (pronation, eversion)
- *Akillestendinit* (sjældnere -ruptur) ses men er hyppigere ved **HLA-B27 associerede artritter**
- *Metatarsofalangealleddene*: Hyppigst angrebne fodled
	- Subluksation
	- Forfodsfald (giver også bredere forfod)
	- Gangbelastning af capitula
	- *Hammertådannelse* (pga. fleksionskontraktur i PIP)
	- Hallux valgus med buniodannelse
- *Tarsaltunnelsyndrom* sjældent: kompression af n. tibialis post. -- smerter og paræstesieri hælregion

###### Columnaaffektion
*er begrænset til columna cervicalis!* Symptomer omfatter *smerter, indskrænket bevægelighed og neurologiske udfaldssymptomer*. Neurologiske symptomer varierer fra milde sensibilitets-/motoriske forstyrrelser til tetraplegi og skyldes tryk op medulla og/eller radices. Evt. kan ses *basilarisinsufficiens* pga. afklemning af a. vertebralis.


Ledforandringer

- Vigtigst: forandringer i atlantoaxiale led
	- Medfører *instabilitet og subluksation*
		- hyppigst subluksation af atlas *fremad på epistropheus*
			- ved foroverbøjning, vippes dens bagover og kan trykke på medulla
		- sjældnere forskydning af arcus bagud (kræver fraktur/destruktion af dens) eller vertikal subluksation (dens kan forskydes op i foramen magnum)   
	- Udgår fra:
		- apofysære led
		- Luschkaleddene
		- Synovialmembranen
	- Lig. transversum atlantis svækkes, bliver slapt
	- Erosioner i *dens* (evt. fraktur, destruktion)   
- Øvrige intervertebralled:
	- Discusforsnævring
	- Subluksationer

Der er parallelitet mellem sværhedsgrad perifert (især hænder) og i cervikalcolumna. Ingen sammenhæng mellem subluksation og neurologiske udfald.

Ved neurologiske symptomer: risiko for progression ved små traumer, whiplash læsioner og intubering.

###### Hofteleddene
inddrages sjældent! Smerter herfra skyldes snarere bursitis trochanterica. *Aseptisk knoglenekrose dog komplikation til langvarig systemisk glukokortikoidbehandling.*

###### Kæbeledsaffektion
giver funktionssmerter og evt. kompromitteret bidfunktion og indskrænket mundåbning.

Inddragelse af artt. cricoarytenoideae kan give hæshed, synkesmerter; sjældnere respirationsbesvær.

Affektion af sternoclavikulære og manubriosternale led: lokale smerter, derudover sjældent af klinisk betydning.

#### Ekstraartikulære manifestationer
Optræder hyppigst hos seropositive ptt. Som regel først efter længerevarende og udbredt ledsygdom.

##### Almensymptomer
kan debutere samtidig med ledsymptomer men også prodromer (uger-måneder). 

- Træthed
- Anorexi
- Vægttab
- Diffuse led- og muskelsmerter
- Subfebrilia (høj feber sjælden -- mistænk infektion)

##### Noduli rheumatici
optræder hos 20-30%. Lejret i subcutis, varierer i størrelse (få mm til 4-5 cm). Oftest indolente.

- Følger som regel udivkling i kronisk artrit (undtaget reumatoid nodulose med forværring af noduli trods godt behandlingsrespons)
- Oftest forskydelige ifht. underlag -- evt. lejret i periost
- Hyppigst lokaliseret periartikulært: steder med tryk
	- Hyppigt 
		- bursa olecrani
		- proksimal ulna
		- occiput
		- akillessenen
	- Sjældnere
		- Stemmebånd
		- Sclerae
		- Lunger
		- pericardium
		- myocardium
- Kan inficeres og give mekaniske og kosmetiske gener

##### Øjenmanifestationer
Hyppigst: sekundært *Sjögrens syndrom*, keratoconjunctivitis sicca, hos 15-20%.

Sjældnere

- episcleritis, scleritis (< 1%); for 50% bilateral
	- Episclerit: godartet, selvlimiterende, undertiden recidicerende manifestation -- kræver ikke behandling
		- akut indsættende kraftig rødme nasalt eller temporalt, ofte uden smerte eller ømhed
		- skelnes fra scleritis ved at injucerede kar kan bevæges ifht. sclera (pga. lokalisation i subkonjunktivalt væv)
	- Scleritis: alvorlig, længerevarende og behandlingskrævende -- kan medføre synsnedsættelse pga. sekundær uveitis, keratitis eller cataract
		- kan være kraftig rødme og udtalte smerter
		- undertiden form af nodulus rheumaticus: sclera bliver tynd og gennemsigtig med af uvea betinget blålig farve: *scleromalaci*. Perforation af sclera, *scleromalacia perforans*, er alvorlig komplikation.

##### Reumatoid vaskulit
omfatter 3 typer:

1. *leukocytoklastisk vaskulit*: primær venoler -- vises ved vaskulær palpabel purpura
2. *nekrotiserende arterit*: små og middelstore arterier -- kan histologisk ikke skelnes fra polyarteriitis nodosa
	- vises ved: perifer polyneuropati
	- arteriitis i GI-kanal med abdominalsmerter, blødninger og infakrtdannelse, sjældnere perforation
	- Nekrotiserende, skarpt udstansede ulcera i huden og finger-/tågangræn
3. *endarteriitis obliterans*: små arterier
	- i fingre og tæer: neglevoldsvaskulitter (infarkter), Raynauds fænomener og akrocyanose
	- Evt. isoleret vasculitis-manifestation -- i fingre ikke intensivt behandlingskrævende
	- Sjældnere: lungearterier. Sjældent i nyrer.

##### Lunger og pleura
påvirkes sjældent, men dog:

- pleuritis
	- almindeligt autopsifund
	- symptomgivende pleuritis, hyppigst ekssudativ, især hos midlandrende mænd med længerevarende sygdom -- kan være debutmanifestation
	- væske: højt proteinindhold, *karakteristisk lavt glukoseindhold*, moderat mononuklær pleocytose (100-5.000/$\mu$l).
	- differentialdiagnoser: infektion (TB) og malign tumor (pleuracarcinose)
- noduli rheumatici i lunger
	- solitære eller multiple
		- *Caplans syndrom* er multiple noduli hos ptt. med pneumokoniose 
	- sjældent symptomgivende
	- differentialdiagnoser: primære og sekundære neoplasier, lungeabscesser
	- kan ses henfald i noduli, undertiden med bronkopulmonale fistler 
- lungefibrose
	- *progredierende, diffus interstitiel lungefibrose* -- kan give restriktiv lungefunktionsnedsættelse
- fibroserende alveolitis og bronchiolitis obliterans
	- sjældent
		- også som evt. komplikation til beh. med
			- penicillamin
			- methotrexat
			- sulfasalazin
- pulmonal arteriit
	- *endarteriitis obliterans* i lungeartier -- resulterer i sekundær pulmonal hypertension og cor pulmonale.

##### Kardiale manifestationer
er sjældent klinisk betydende. Almindeligst er

- pericarditis (hyppigt autopsifund)
- hjertetamponade (meget sjældent)
- ledningsforstyrrelser (pga. *nodulær myocarditis*), klaplidelser pga. reumatisk valvulit og aortit samt *koronar arterit* med sekundært AMI ("er beskrevet")

##### Neurologiske manifestationer
er vigtigst trykneuropatier og polyneuropatier

- Trykneuropatier
	- karpal- og tarsaltunnelsyndrom
	- se desuden `Karakteristiske regionale forandringer i bevægeapparat`
- Polyneuropati
	- Muligvis betinget af vasculitis i nutritive kar
	- To typer:
		1. Godartet, *distal sensorisk neuropati*: paræstesier, sensibilitetsforstyrrelser distalt på fingre og tæer
		2. *Mononeuritis multiplex*: svære sensoriske og motoriske udfaldssymptomer, herunder dropfoot og drophand

##### Mavetarmkanal
er påvriet ved gsatritis og ulcus pga. NSAID-behandling.

##### Renale manifestationer
skyldes hyppigst nefrotoksiske farmaka eller NSAID.

Sjældent *glomerulonephiritis* som sjælden ekstraartikulær manifestation. *Sekundær amyloidose* kan være senkomplikation (incidens af amyloidose ved autopsi 5-20%).

##### Feltys syndrom
omfatter triaden RA, splenomegali (+/-) og neutropeni. Undertiden trombocytopeni, sjældnere hæmolytisk anæmi. *Vigtigste komplikatino er infektioner*.

Hyppigst hos ptt. med længervarende RA og andre ekstraartikulære manifestationer.

- *Ulcera på crura* særligt karakteristisk
- Brunlig pigmentering af hud på ekstremiteter
- I serum findes IgM-RF og granulocytspecifikke kerneantistoffer hos halvdelen.

##### Lymfeknudesvulst
kan være ret udtalt, hyppigst ved drænage af synovitis-områder. Generaliseret lymfeknudesvulst er usædvanlig.
	
### Parakliniske fund

#### Blodanalyser
- *Sænkningsreaktion* ved aktiv RA (pga. akutfasereaktanter, anæmi og/eller hypergammaglobinæmi). Kumulativ CRP-forhøjelse over tidkorreleret til udvikling af knogleerosioner.
- *Hæmoglobin*: Moderat anæmi (6 mM) hyppig i aktiv fase. Slem anæmi tyder på blødning e.a.
	- Normocytær, normokrom eller hypokrom.
	- S-Jern, P-Transferrin kan være nedsat
	- S-ferritin (akutfasereaktant) kan være forhøjet. Lave værdier tyder på jernmangelanæmi
- *Leukocytter*: Moderat neutrofil granulocytose i aktive faser og v. glukokortikoidbehandling
- *Granulocytopeni*: Ses ved Feltys syndrom pga. sekvestrering i milt. Knoglemarvssuppression hos 1% beh. med DMARD (sulfasalazin, guldsalte, leflunomid, cytostatika)
- *Let eosinofili*: Aktiv RA, associeret til guldsalt-beh.
- *Trombocytose*: Kan være udtalt i aktive faser. *Trombocytopeni* i let-moderat grad v. Feltys syndrom og som DMARD-bivirkning
- *Albumin*: Ofte nedsat
- *Leverenzymer*: Let-moderat forhøjet basisk fosfatase hyppigt ved aktiv sygdom. Desuden leverpåvirkning ved behandling (DMARD, NSAID)
- *Kreatinin*: Bør monitoreres, især v. beh. med nefrotoksika
- *Immunglobuliner*: IgG-forhøjelse uden tegn på aktiv inflammation -- særligt ved sekundært Sjögrens syndrom.
- *Reumafaktorer*: Over 70% bliver IgM-RF-positive i løbet af sygdom. Samtidig IgA-RF øger diagnostisk specificitet.
	- Også hyppigt tilstede ved Sjögrens, SLE, sklerodermi. 5% af befolkningen er positive.
- *Antinukleære antistoffer*: Findes hos 80% -- halvdelen organuspecifikke i lav titer, halvdelen granulocytspecifikke antinukleære antistoffer (komplementbindende ved Feltys syndrom)
- *Anti-CCP*: Meget specifik (95%) markør, selv tidligt. God prædiktor for radiologisk progression. 
- *Komplement*: Kan være forhøjede (akutfase). Splitprodukter (fx C3d) kan være forhøjede ved aktiv sygdom.

#### Urinanayser
Beh. kan være nefrotoksisk (guldsalte, AZA) medførende glomerulonephritis. Sædvanligvis reversibel, men proteinuri kan vare måneder. 

Cyclophosphamidbehandling kan give kemisk cystitis med varierende grad af erytrocyturi.

#### Ledvæskeanalyser
Indiceret ved uafklaret diagnose for at udelukke septisk artrit eller krystalartrit.

#### Billeddiagnostik
##### Røntgen
- Ikke specifik, men karakteristiske fund er vejledende
- Serierøntgen af hænder, håndled, fødder med 6-12 mdrs. interval anvendes til monitorering (vurderes ved Sharp-score)
-  Hyppigste og tidligste forandringer optræder i små  led på hænder og fødder, særligt tidligt: knogleforandringer i 5. metatarsofalangealled samt 2. og 3. MCP-led.
-  Radiologisk forandringer omfatter
	- Bløddelshævelse og ledansamlinger
	- Regional osteoporose
	- Ledspalteforsnævring pga. diffus ledbruskdestruktion
	- Erosioner, efterfølges af obliteration af ledspalte og udbredt leddestruktion
		- Særligt karakteristisk: tidlige marginale erosioner på grænse ml. knogle og ledbrusk
		- Endvidere: erosioner i relation til tenosynovitis, ved ligamenttilhæftninger
	- Subkondrale cystelignende læsioner, pseudocyster, pga. invaderende granulationsvæv
	- Leddeformitet, fejlstillinger og total leddestruktion
	- I columna cervikalis
		- Forsnævring af disci; erosioner og destruktioner i Lushcha-leddene og apofysære led; subluksation af cervikalhvirvler, især C4-C6
		- Atlantoaxial subluksation (> 3mm mellem arcus ant. atlantis og dens) og instabilitet (funktionsundersøgelse); erosioner i atlantoaxiale led og dens; evt. fraktur og destruktion af dens; evt. subluksation af dens i foramen magnum 
- *Der ses ikke knoglenydannelse* (modsat HLA-B27-associerede artritter og psoriatisk artrit, osteoartrose)  

##### MR-scanning
- Erosioner ses tidligere end ved røntgen.
- Tillader gradering af inflammation
- Indgår i ACR/EULAR 2010-klassifikationskriterier pga. påvisning af involverede led
- Kan påvise ekstraartikulære forandringer
	- tenosynovit
	- tendinit
	- bursit
	- seneruptur
- Osteitis (knogleødem) uafhængig prædiktor for strukturel ledskade

##### Ultralyd
- Velegnet til påvisning af bløddelsforandringer og væskeansamlinger (tendinitter, bursitter, cyster etc.)
- Indgår i ACR/EULAR 2010-klassifikationskriterier pga. påvisning af involverede led

##### Knoglemineralmåling (DXA-scanning)
benyttes, bl.a. til kontrol hvert 2.-3. år for osteopeni- eller osteoporoseudvikling

##### Knoglescintigrafi
er vejledende ved usikkerhed om tilstedeværelse af inflammation. Mærkede leukocytter kan påvise infektion.

### Vurdering af sygdomsaktivitet
Bedømmelse af aktivitet er grundlag for behandlingsstrategi og for vurdering af invaliditetsgrad og prognose. Vurderingen går på:

- Antal hævede led
- Antal ømme led
- Patientens vurdering af ledsmerte*
- Patientens globale vurdering af sygdomsaktivitet*
- Undersøgerens globale vurdering af sygdomsaktivitet*
- Måling af akutfase respons (CRP)
- Funktionsevne (Health Assessment Questionnaire)
- Radiologisk vurdering

(* = *Visuel Analog Skala*.) For flere detaljer, se. s. 386.

### Forløb og prognose
Kronisk sygdom -- progredierende og irreversibel funktionsnedsættelse og øget mortalitet. Tre forløbsformer findes:

- Ptt. med *enkelt eller få måneders varende aktivitetsperioder* med langvarig (evt. permanent) remission uden betydende funktionsnedsættelse. 15-20%.
- Ptt. med *intermitterende, måneder varende aktivitetsperioder* afløst af remissioner (beh. seponeres). Remissioner varer længere end sygdomsperioder, hos halvdelen > 1 år. 15-20%.
- Ptt. med *progredierende sygdom*. Hyppigst - 60-70%. Enten i form af
	- langsom, jævn progression
	- progression baseret på periodiske forværringer med stadig inkomplet remission.

Funktionsniveau kan inddeles i 4 klasser:

1. Patienter er selvhjulpne, stort set velbefindende
2. Patienter er selvhjulpne, men med besvær
3. Patietner må have hjælp af andre til daglige funktioner
4. Patienter er totalt invaliderede, bundne til seng eller kørestol.

Knogleeerosioner og funktionsnedsættelse udvikles især over de første 5-6 år, særligt 2 første.

##### Erhvervsevne
efter 10 år har 50% mistet erhvervsevne

##### Dødelighed
hos ptt. med langvarig RA er ca. dobbelt så høj som normalbefolkningen.

- 15-20% direkte relateret til RA (vasculit, atlantoaxial subluksation, lungelidelser)
- størstedelen relateret til RA-varighed (især 50-70-årige): kardiovaskulræe sygdomme, infektioner, GI-blødning/-perforation

##### Prognose
kan ikke forudsiges ved debut. Markører for dårlig senprognose mht. funktionsklasse og dødelighed er

- Høj IgM-RF- og anti-CCP-titer
- vævstype HLA-DRB1
- MR-knogleødem
- tidlig og hurtig udvikling af erosioner
- hurtiginddragelse af mange led
- hurtig udvikling af betydelig funktionsnedsættelse
- ekstraartikulære manifestationer
- rygning
- lav social status 

### Diagnose
Kriterier s. 373 (tab. 24.14)

### Differentialdiagnoser
Kan være vanskelige i ikke-erosivt stadium.

- Systemiske bindevævssygdomme: SLE, mixed connective tissue disease
	- Adsiller sig ved ekstraartikulære manifestationer og serologi.
	- Klinisk kan artrit ved SLE være uadskillelig fra RA, men denne er *ikke-erosiv*.
- Polymyalgia rheumatica
	- Neg. polymyalgi-biopsi beh. med systemisk glukokortikoid kan vise sig at have RA
- Kæmpecellearteritis
	- Kan ledsages af seroneg. artrit -- hyppigst oligoartrit i store led
- Virusassocierede reaktive artritter
	- Perifer, symmetrisk polyartikulær ledaffektion (sv.t. RA).
	- Oftest forbigående (få uger) -- RA kræver min. 6 uger
	- Parvovirus B-19, hepB+C, endemisk forekommende $\alpha$-virus, rubella, parotitis m.fl.
	- Diagnose hjælpes af antistofbestemmelser.
- Krystalartrit
	- *Arthritis urica*
		- Kan i kroniske stadier blive polyartikulær -- dog *forudgået af længere periode med recidiverende, akutte monoartritter.
		- Patognomonisk: uratkrystaller. Desuden seronegativ.
	- *Pyrofosfatartropati*
		- Kan have polyartikulær forløbsform sv.t. RA.
		- Pyrofosfatkrystaller i ledvæske diagnostisk. Chondrokalcinose suggestivt.
- Borreliaartrit
	- Kan være kronisk polyartrit, oftest oligoartrit i store led.
	- Vigtige at finde da beh. er antibiotika.
- Psoriasisk polyartrit
	- Oftest *asymmetrisk* men kan have topografisk mønster som RA.
	- Definition: Seroneg. pt. med psoriasis.
- HLA-B27-associerede reaktive artritter
	- Asymmetrisk ledaffektion -- næsten altid større led, især i UE.
	- Karakteristiske ekstraartikulære manifestationer.
- Enteropatisk artropati
	- Oligoartrit, oftest store led. Aktivitet følger tarmsygdommens.
- Spondylitis ankylopoietica
	- Spondylit er dominerende, særligt thoracolumbalt.
	- Perifer artrit især større led, særligt hofteled.
- Artrit associereret med malign sygdom
	- Vigtigst: Hypertrofisk osteoartropati: Oligoartrit i store led, oftest knæ, samt trommestikfingre. Obs. lungetumor.
- Hyperlipoproteinæmi, særligt type I og IV
	- Sjældent kronisk, seroneg. polyartrit -- oftest oligoartrit
- Osteoarthrosis polyarticularis
	- Forveksles ofte med RA.
	- Symmetrisk ledaffektion inddragende PIP- og DIP-led samt tommelfingerens rodled (*ikke MCP-led*).   

### Behandling
Se figur s. 391, desuden Kompendiet.

1. valg: Methotrexat
2. valg: Sulfasalazin / Klorokin / Leflunomid

Ved svigt biologisk behandling + MTX

Desuden injektion af glukokortikoid i led -- nødigt oftere end hver 3. måned.

## Spondylartritter
Inflammatoriske lidelser involverende rygsøjlen, men ofte også perifere led (asymmetrisk oligoartrit især i UE) og enteser. Er seronegativ.

Desuden affektion af øjne, hjerte, hud, lunger, GI-kanal, og der ses familiær ophobning og HLA-B27-association.

### Spondylitis ankylopoietica

#### Synonymer
Ankyloserende spondylit (AS), Bechterews sygdom, Marie-Strümpells syndrom, pelvospondylitis ossificans

#### Definition, karakteristik og klassifikation
- Ukendt årsag. Stærkt associeret til HLA-B27, men ikke autoantistoffer.
- Kendetegn:
	- Afficerer columna (spondylitis) og SI-led (sacroiliitis) men også ofte store led (hofte, skulder, knæ)
	- Ekstraskeletale manifestationer: anterior uveitis, aortaklapinsufficiens, apikal lungefibrose

###### Diagnosticeres
i tidlige stadier ved ASAS-kriterier:

- Rygsmerter > 3 mdr, symptomdebut < 45 år samt:
	1. Sacroiliitis påvist ved rtg eller MR + mindst ét af nedenstende kriterier *eller* 
	2. HLA-B27-positivitet + mindst 2 af nedenstående kriterier

	- Inflammatoriske rygsmerter
	- Arthritis
	- Entesitis (hælsmerter)
	- Uveitis
	- Dactylitis
	- Psoriasis
	- Crohns sygdom/Colitis ulcerosa
	- Effekt af NSAID
	- Forekomst af SpA i familien
	- HLA-B27-positivitet
	- Forhøjet CRP

#### Forekomst
- 0,1-0,5% af voksen nordeuropæisk befolkning. 
- Op til 20% af HLA-B27-positive med familiehistorie for AS. 
- Mand:kvinde-ratio 3:1

#### Ætiologi, patogenese og patologisk anatomi
- Ukendt ætiologi, men genetisk faktor: 90-95% har HLA-B27 (9% i baggrundsbefolkningen)
- *Entesitis* er central patologisk proces -- delvist medieret af TNF-$\alpha$.
	- Juxtaartikulære og ekstraartikulære enteser, især SI-led, discovertebrale, costovertebrale og costosternale led, manubriosternal- og sternoclavikulærled, paravertebrale ligamenter, samt enteser lokaliseret til crista iliaca m.fl. (p. 398)
- Inflammation medfører knogleerosion og halisterese efterfulgt af knoglenydannelse: mellem corpora vertebrae kaldes disse *syndesmofytter* 
- Resultat er stivhed og bevægelsesindskrænkning (ved RA instabilitet, ledløshed).

#### Symptomer og kliniske fund
Kan inddeles i to grupper: bevægeapparatrelaterede og ekstraskeletale manifestationer:

- bevægeapparatrelaterede (især aksialt skelet)
	- Rygsmerter: debut hos 75%.
		- Lavt i lænden, glutealregionerne. 
		- Udvikles langsomt, ledsaget af stivhed og bevægelsesindskrænkning. 
		- Kan være radierende men uden neurologiske udfald -- evt. *falsk positiv Lasègues prøve*.
		- Forværres i hvile, forstyrrer ofte nattesøvn. Varierende grader af morgenstivhed.
		- Flere og flere led inddrages og kan give fx respirationssmerter.
		- Udvikling af fejlstillinger.
	- Perifer artrit (30-40%, tidligt kun 20%)
		- Oftest hofte-, skulder- og kæbeled
		- Kronisk, asymmetrisk oligoartrit især store led i UE, evt. destruktiv (særligt hoften: fleksionskontraktur)
	- Bløddelssmerter (acillessenetenditis, plantar fasciitis, dactylitis mm.), fx betinget af entesitis.  
- ekstraskeletale manifestationer (op til 25%)
	- Akut anterior uveitis (iridocyclitis): op til 25%, især HLA-B27-positive. Ensidig, recidiverende. Skifter evt. øje. Akut debut med: smerter, tåreflåd, fotofobi, sløret syn.
	- Kardiovaskulære symptomer (4-10%, især ved perifer artrit): ascenderende aortitis, aortaklapinsufficiens, ledningsforstyrrelser, epericarditis.
	- Apikal lungefibrose (øget risiko for luftvejsinfektion)
	- Subkliniske Crohnlignende forandringer i terminal ileum, colon
	- Øget frakturrisiko af columna ved ankylotiske forandringer med øget risiko for dislokation og læsion af medulla.
	- Sjældent IgA-glomerulonephritis og amyloidose.

#### Parakliniske fund
- Akutfasereaktanter: CRP/SR
- "Anæmi ved kronisk sygdom"
- Forhøjet P-IgA (bidrager til sænkningsforhøjelse)
- *Ikke* autoantistoffer
- Evt. HLA-B27-vævstype
- Billeddiagnostik
	- Røntgen, udvikles over år:
		- Sacroiliitis: symmetriske -- tidligt uregelmæssige ledspalter; senere småerosioner (evt. savtakkede), sklerosering af underliggende knogle; til slut ankylose (sammenvoksning) 
		- Enteser: Erosioner og knoglenydannelse
			- Columna: Tidligt *Romanus læsioner* (småerosioner i forreste hjørner af corpora sv.t. enteser); senere knoglenydannelse (*shiny corners*) førende til ændret form (*squaring*); til sidst *syndesmofytter* (sammenvoksning)
		- Perifere led: Sjældent radiologisk påviseligt, oftest i hofteled. Destruktive forandringer og ankylose; erosioner og sklerosering.
	- MR: Kan påvise forandringer før rtg ved påvisning af knoglemarvsødem og subkondral osteitis.

#### Sygdomsforløb og prognose
- Sygdom er kronisk med variende aktivitet.
- Nogle tilfælder forbliver lokaliseret til SI-led, mens andre har spondylitis og ekstraartikulære manifestationer
- Øget risiko for at forlade arbejdsmarkedet samt *øget mortalitet* (kardiovaskulær morbiditet og columnafrakturer).
- Ingen kliniske eller parakliniske markører for prognose.

#### Diagnose og differentialdiagnoser
- Diagnose stilles pga. gradvis og snigende udvikling ofte først efter måneder/år.
- Debuterer sjældent efter 40-års alderen.
- Sacroiliit er obligatorisk.
- Debutsymptomer (i faldende hyppighed):
	- Smerter, stivhed i ryg og glutealregioner med inflammatorisk præg (forværring i hvile, lindring ved aktivitet)
	- Asymmetrisk oligoartrit især i UE
	- Entesopatier
	- Ekstraartikulære manifestationer
		  
##### Differentialdiagnoser
- **Degenerative ryglidelser**: Debuterer akut, ingen familiær forekomst, smerter aftager i hvile og forværres af bevægelser, ischias er ledsaget af neurologiske udfald.
- **Spondylose**: Radiologiske fund -- udløbere (*osteofytter*) er horisontale, mens SpAs *syndesmofytter* er vertikale; ikke SI-involvering
- **Diffus idiopatisk skeletal hyperostose**: Paraverterbrale forkalkninger uden SI-involvering eller øget forekomst af HLA-B27
- **Tuberkuløs spondylitis**: Svær kyfose (gibbus). Rtg viser destruktion af corpora uden syndesmofytter.
- **Maligne lidelser** : Oftest ældre, vil kunne påvise malignitet ved undersøgelse

#### Behandling
- Fysioterapi
- Medikamentelt
	- Ingen kausal behandling
	- Formål: reduktion af smerter og stivhed
	- Farmaka
		- Simple analgetika og NSAID
		- Sulfasalazin ved perifer artrit
		- *Ingen langsomtvirkende gigtmidler (DMARDs) fra RA har effekt*
		- Kortvarig peroral glukokortikoid ved aktiv sygdom; lokal injektion kan benyttes med langvarig effekt
		- Biologisk behandling (TNF-$\alpha$-hæmmere): Forbedrer smerter, bevægelighed og funktionsevne; reducerer perifere synovitter, entesitter og inflammation. *Mindsker ikke udvikling af syndesmofytter eller ossifikationer*.
- Kirurgi 

### Psoriasisartrit
- Særligt karakteristisk er DIP-led
- Oftest asymmetrisk oligo- eller monoartrit (større led), evt. symmetrisk polyartrit (uskelnelig fra RA bortset fra DIP-led, reumafaktor-negative), aksial artrit.
- Reuma-neg, evt. anti-CCP-pos. 
- Divers ledaffektion
- 15% diagnosticeres før hudforandringer
- Typisk mere benign end RA. 
- Blandet erosive og proliferative forandringer ved billeddiagnostik
- Behandling er NSAIDs og DMARDs. 

## Infektionsrelaterede artritter
### Infektiøs artrit
#### Synonymer
Septisk artrit, bakteriel ledbetændelse, artritis pyogenes

#### Definition, karakteristik, klassifikation
- Skyldes kolonisering af led og lednære strukturer med patogene mikroorganismer
- Oftest *monoartikulær*
- Særligt udsatte led er led allerede sæde for kronisk synovitis (RA, artritis urica mm.)
- Mortalitet 10-15%, varig ledskade hos 25%.

#### Forekomst
Hyppigst ved *lokal* (forudbestående ledsygdom) og *generel* (fx immunsuppression) resistensnedsættelse. Dette gælder dog ikke for @gonoroisk artrit*.

#### Ætiologi
- *Staph. aureus*: Henved halvdelen. Ældre patienter, ofte forudbestående ledsygdom, især RA
- Gramnegative stave: Knap halvdelen. Ældre, immuninkompetente; beh. med glukokortikoid/immunmodulerende stoffer, kroniske lever- og nyrelidelser, diabetes
	- *E. coli*, *Klebsiella*, *Ps. aeruginosa*, *Salmonella*. 
- *H. influenzae*: Børn, ofte samtidig meningitis
- Pneumokoker: Større børn, ofte samtidig otitis media og pneumoni
- Gonokokker: 15-35 år, oftest kvinder. Sjældent.
- Andre
	- Borrelia: alle aldre, forudgående skovflåtbid
	- Mycobakterier: alle aldre, tidligere TB, etnisk baggrund i endemiske områder. Ofte subkronisk
	- Anaerobe: Luftlomme, ildelugtende synovialvæske. Ofte blandingsflora.

#### Patogenese
Tre mekanismer:

1. Hæmatogent
2. Penetrerende læsioner af huden
3. Spredning fra lednære strukturer

#### Patologisk anatomi
Inficeret synovialmembran er hypertrofisk, hyperæmisk, overfladen belagt med fibrintjavser, områder med nekrose.

#### Symptomer og kliniske fund
- Akut opstående, smertefuld hævelse af ét enkelt led, ofte med rødme af overliggende hud
- Bevægelsesindskrænkning, smerter ved aktiv og passiv bevægelse
- Især *store* led (knæ-, hofte- (hos børn), hånd- og fodled) 
	- Aksiale led rammes sjældent, især hos stofmisbrugere.
- Ekstraartikulære symptomer:
	- Feber
	- Evt. primært infektionsfokus

#### Parakliniske fund
- *Ledaspiration bør foretages hos alle patienter*.
- 25% har bakteriæmi
- Neutrofil leukocytose, evt. trombocytose, akutfaseforhøjelse

#### Prognose
Mortalitet 10-15%, varig ledskade hos 25%.

#### Diagnose og differentialdiagnoser
Diagnosen stilles ved påvisning af bakterien i ledvæsken.

##### Differentialdiagnoser  
- Akut krystalsynovitis (især urica): Påvises ved krystaller i ledvæske
- Osteoartrose: Fravær af/beskeden akutfasereaktion; steril, cellefattig ledvæske. Radiologiske forandringer.
- Hæmarthron: Diagnosticeres ved ledpunktur.

#### Behandling
Antibiotika, drænage, immobilisering, evt. synovektomi.

Initielt valg af antibiotika er bredt virkende -- fx cefalosporin (cefuroxim). Beh. 2 uger parenteralt, herefter 4-6 uger peroralt.

#### Særlige former
- Gonokokartrit.
	- God prognose ved rettidig behandling.
	- Overvejende unge, seksuelt aktive og raske kvinder.
	- 3 varianter
		1. Akut polyartrit med tenosynovit. 2/3 febrile. Makulopapuløst udslæt, indolente småpustler på truncus og ekstremiteter. Som regel gonokokbakteriæmi.
		2. Monoartrtitis. Hyppigst knæled men også andre større led.
		3. To-faset. Startende med migrerende polyartrit, gradvis koncentrering i ét led.
	- Diagnosticeres ved påvisning af bakterien. Bakterien er følsom for miljøskift, så hastig behandling.
	- Behandling: ceftriaxon, cefotaxim, cirofloxacin.
	- Differentialdiagnoser: Septisk artrit (andre former), Reiters syndrom (reaktiv artrit)
- Bakteriel endokorit
- Meningokokbakteriæmi med eller uden menigitis
- Tuberkuløs artrit
- Borrelia-artrit
- Svampeartrit
- Arthritis i protesebærende led
- Infektiøs bursitis

### Infektiøs spondylitis
Se `infektiøs artrit`. 

- Spredes hyppigt til nabohvirvler via disci.
- Hyppigst grampositive bakterier, kan dog også skyldes gramnegative og mycobakterier. Sjældent svampe.
- \> 50% *S. aureus*, 25% gramneg. (*E. coli*, *P. aeruginosa*)
- Især hos ældre, kronisk syge og immunsvækkede.
- Hyppigst hæmatogen spredning, evt. direkte infektion (kirurgi).
- Mortalitet op til 10%.

#### Symptomer
- Rygsmerter hyppigst. Oftest lumbalt, tuberkuløst dog thoracalt.
	- Bevægeindskrænkning
	- Bankeømhed 
- Nervepåvirkning.
- Evt. septisk springende temperatur og medtaget almentilstand.
- Evt. abscesudvikling
- Behandling: Antibioitika og immobilisering -- se `Infektiøs artrit`.

#### Parakliniske fund
Som infektiøs artrit. Desuden evt. forhøjet basisk fosfatase; hypercalcæmi sjældent. Radiologiske fund ofte ved debut grundet langsom udvikling af symptomer.

#### Diagnose og differentialdiagnoser
Diagnose stilles ved påvisning af bakterier i columna.

##### Differentialdiagnoser
- Degenerative ryglidelser og myogene smerter: Uden akutfasereaktion.
- Ved akut indsættende smerter: fraktur: Ingen akutfasereaktion.
- Maligne tumorer: Biokemi -- forhøjede basiske fosfataser, hypercalcæmi, manglende tegn på tumor

### Reaktive artritter
#### Synonymer
Postinfektiøs artrit

- Akut synovitis, opstår få uger efter infektion (hyppigst GI- eller urogenitalinfektion)
- Kan *ikke* påvises mikroorganismer i led.
- Inddeling
	1. HLA-B27 associerede artritter
		- Hyppigst asymmetrisk oligoartrit (knæ, ankler, fødder -- men også asiale led)
		- Ekstraartikulære manifestationer: hud, slimhinder og entesopati.
		- Efter GI- eller urogenitalinfektion. Hyppigst intracellulære bakterier:
			- *C. trachomatis* 
			- *Y. enterocolitica*, *Y. pseudotuberculosis*,
			- *S. enteritidis*, *S. typhimurium*
			- *C. jejuni*
			- *S. flexneri*
		- Inkluderer Reiters syndrom (artrit, conjuctivit, urethrit)
	2. ikke-HLA-B27 associerede artritter
		- Febris rheumatica
		- Borrelia-artritis
		- Virale artritter
- Patogenese: Krydsreaktion mellem mikroorganismens antigener og autoantigener. Evt. immunkompleksaflejring i synovialmembran.

#### Symptomer
- 1-3 uger efter akut enteritis eller urethritis
- Ofte vekslende temperatur
- Oftest oligoartikulær, særligt i UE, asymmetrisk.
- Entesitis, radiologisk: hælsporer.
- Ekstraartikulære manifestationer
	- Reiter syndrom.
	- Øjne: conjunctivit, anterior uveit
	- Se desuden s. 417.

#### Parakliniske fund
Uspecifikke -- infektion med plausibel mikroorganisme er vejledende.

#### Forløb og prognose
Ofte recidiverende (især HLA-B27-positive), sjældent kronisk eller akut. Fortsat asymmetrisk.

#### Differentialdiagnoser
- Infektiøs artrit: Oftest monoartikulær (gonoroisk artrit undtaget)
- RA: IgM-reumafaktor
- Psoriasisartrit: Kan være umulig at skelne klinisk. Forudgående infektion eller typiske ekstraartikulære manifestationer taler for reaktiv artrit.

#### Behandling
Symptomatisk (ekssudatudtømning, intraartikulær glukokortikoid). Evt. også partnere (hvor relevant).

Artrit i mere end 6 mdr. kan forsøges behandlet med salazopyrin eller metotrexat men sparsom dokumentation. TNF-$\alpha$-inhibitorer effektive.

### Febris rheumatica
- Efter infektion med gruppe A, $\beta$-hæmolytiske streptokokker (reumatogene, M-serotyper med stor hyaloronsyrekapsel)
- Involverer led, hjerte, hud, CNS og subcutis.
- Fremherskende symptomer: *polyarthritis, pancarditis, subkutane noduli, erutehma marginatum* og *Sydenhams chorea*.
	- Major-kriterier: 
		- **Pancarditis** (80-90%)
		- Polyarthritis (asymmetrisk oligo- eller polyartrit, migrerende. Knæ, ankler, evt. albuer/håndled). *Ikke destruktiv* men MCP-led kan få ulnar deviation pga. periartikulær tenosynovit og fibrose (Jaccoud-led)
		- Sydenhams chorea: Abrupte, formålsløse, ufrivillige bevægelser. Muskelsvækkelse. Emotionel ustabilitet.
		- Sjældnere subkutane noduli (faste, indolente. Især albue, underarms strækkesider), erythema marginaum
	- Minor-kriterier: Feber, CRP-/SR-forhøjelse 
- Ikke HLA-B27-associeret.
- Sjælden i vestlige lande.
- Antages at skyldes postinfektiøs autoimmun proces.

### Virusartrit
- Polyartrit, svinder spontant i løbet af uger. På OE.
- Oftest symmetrisk. 
- Vira:
	- Hepatitis B og C
	- Rubellavirus
	- Parvovirus
	- HIV-relateret artropati

## Metabolisk artrit. Krystalsynovitis
### Arthritis urica
#### Synonymer
Urinsyregigt, podagra

#### Definition, karakteristik, klassifikation
- Recidiverende *monoartrit* -- efter flere år evt. kronisk mono- eller polyartrit 
- Udløses af uratudfældninger, ofte ledsaget af *hyperurikæmi*
- Ekstraartikulære urataflejringer (tophi), urolithiasis og interstitiel nefropati især ved kronisk arthritis urica.
- 4 faser:
	1. Asymptomatisk hyperurikæmi 
	2. Arthritis urica acuta
	3. Intermitterende mono- eller polyartikulær artrit; den interkritiske fase
	4. arthritis urica chronica tophosa

#### Forekomst
0,2-1,4% af voksen befolkning, hyppigere med alderen. P-Urat < 0,42 mmol/L -- incidens < 0,1%; P-Urat > 0,54 mmol/L -- incidens 5%, persisterende i dette niveau 20-30%.

#### Ætiologi, patogenese, patologisk anatomi
For årsager se tabel s. 423. Overordnet kan siges, det skyldes øget produktion eller nedsat udskillelse af urat.

- Øget produktion: idiopatisk, kemoterapi, hæmolyse, alkohol, overvægt, svær psoriasis
- Nedsat udskillelse: nefropati, lægemidler (diuretika, alkohol mm.), hypertension, metabolisk og hormoner

Interleukin-1$\beta$ er af central betydning, idet dannelse i makrofager stimuleres af optagne uratkrystaller. IL-1$\beta$ inducerer TNF-$\alpha$-dannelse.

#### Symptomer, kliniske fund
- **Asymptomatisk hyperurikæmi**: Ingen symptomer, kun få udvikler urica.
- **Akut arthritis urica-anfald**: 
	- Optræder i 40-60 år års-alder   
	- Næsten altid monoartikulært, ofte i storetåens grundled (*podagra*). De fleste øvrige led undtaget skuldre, hofte og aksialt skelet.
	- Vågner om natten med smerter, meget ømt for berøring.
	- Leddet er *varmt, rødt og hævet* ikke sjældent med periartikulært ødem og feber.
	- Varer ofte 1-8 dage
- **Interkritisk fase**: Fasen mellen anfald. Varierer i varighed.
- **Kronisk tophøs arthritis urica**:
	- Ikke længere anfald, men kronisk i store og små led.
	- Gennemsnitligt 12 år efter debut
	- *Tophi*: Hvide "pletter" i hud indeholdende uratkrystaller. Ofte på fingres dorsalsider, hænder, knæ og storetåens grundled.
	- Patienter kan invaliders af fejlstillinger og bevægelsesindskrænkning pga. leddestruktion samt tophøse aflejringer.

#### Parakliniske fund
Intracellulært lejrede, nåleforemde krystaller i ledvæske fra patient med monoartrit er diagnostisk.

Ofte let neutrofil leukocytose og forhøjet P-kreatinen. P-Urat ofte forhøjet, men fadler under anfald.

Ved kronisk sygdom udvikles *billetklip* i subkondral knogle.

#### Forløb, prognose, komplikationer
5-10% udvikler kronisk polyartrit (inkl. tophi og noduli rheumatici). Evt. infektion af tophi.

Leddestruktioner sv.t. RA.

#### Diagnose og differentialdiagnose
Akut opstået monoartrit i storetå næsten altid urica. Ved andre led: aspiration for undersøgelse af krystaller og bakterier.

##### Differentialdiagnoser
- *Septisk artrit* er vigtigste differentialdiagnose.
- *Osteoartrose* kan volde vanskeligheder (også ældre, ofte storeåens grundled)
- *Nodulær rheumatoid arthritis*

#### Behandling
- Akut anfaldsbehandling
	- NSAID
	- Evt. paracetamol (max 4g/døgn) eller tramadol (max 400 mg/døgn).
	- Kolkicin: Ved pt. der ikke tåler NSAID eller virkning utilstrækkelig. Bivirkninger: kolikagtive mavekramper, diaré.
	- Artrocentese, intraartikulær glukokortikoid
	- *Ikke* uratsænkende behandling -- udsving op/ned i P-Urat kan forlænge/inducere anfald. 
- Forebyggelse af recidiver
	- Indiceret ved > 2 anfald af urica med persisterende hyperurikæmi
		- For at forebygge kronisk tophøs sygdom, nefropati og urolithiasis
	- Allopurinol: hæmmer xanthinoxidase, 300 $\rightarrow$ 600 mg; *øget risiko for anfald de første måneder*
	- Probenecid: hæmmer reabs., 250 mg $\times$ 2 $\rightarrow$ 500 mg $\times$ 2, *øget risiko for anfald de første måneder*

### Pyrofosfatartropati
#### Definition og karakteristik
Kan optræde

- akut, episodisk, ofte *monoartikulær* artrit 
- kronisk *polyartikulær* form

Sygdom skyldes aflejring af pyrofosfatkrystaller og kan medføre Økondrocalcinose* (forkalkninger i ledbrusk og menisker).

#### Forekomst
- Halvt så hyppigt som arthritis urica (dvs. 0,1-0,7% af befolkning)
- Debut ofte efter 60 år; familiære former i 20-30 års alderen, ofte polyartikulær.
- Øget hyppighed ved hyperparathyroidisme, hypothyroidisme, hæmokromatose og hypomagnesiæmi

#### Ætiologi og patogenese
Krystaller er inflammationsudløsende som ved urica, evt. via immunglobulin-coating.

#### Symptomer og fund
- Akut, intermitterende variant:
	- Oftest monoartikulær, evt. oligo
	- Oftest knæleddene, sjældnere ankel- og håndled, yderst stjældent storetåens grundled
	- Anfald varer 8-10 dage
	- Anfald kan ledsages af feber og kulderystelser, evt. forårsaget af traumer inkl. kirurgi
- Kronisk, polyartikulær form
	- Kan imitere RA ("pseudo-reumatoid artrit"): Reumafaktor-negativ, kan påvise chondrocalcinose i min. 1 led, samt forekomst af pyrofosfatkrystaller i ledvæske

#### Diagnose og differentialdiagnose
Mistænkes hos ældre med intermitterende mono- eller oligoartrit og påvisning af knæledsbrusk- eller meniskforkalkninger. 

Diagnose hviler på påvisning af pyrofosfatkrystaller i ledvæske: Rombeformede, sjældent talrige. Findes intra- og ekstracellulært og i relation til vævsfragmenter.

##### Differentialdiagnoser
- Infektiøs artrit: I ledvæske ses kraftig neutrofil leukocytose (sv.t. infektiøs artrit), hvorfor ledvæske undersøges bakteriologisk
- Arthritis urica: Urica rammer ofte storetåens grundled, pyrofosfatartrit kun sjældent. Ellers kan krystalpåvisningen skelne.

#### Behandling
NSAID efter samme retningslinjer som urica. Evt. supplement med intraartikulær glukokortikoid -- ved svær polyartikulær artrit evt. kortvarig systemisk glukokortikoid.

## Inflammatoriske bindevævssygdomme
### Sjögrens syndrom
#### Definition, karakteristik og klassifikation
- Systemisk autoimmun sygdom karakteriseret ved kronisk, inflammatorisk eksokrinopati i spytkirtler og/eller tårekirtler førende til ophør af sekretion (*xerostomi* hhv. *keratoconjunctivitis sicca*), evt. andre eksokrine kirtler og andre væv
- Kan være primær eller skundær (RA, SLE, sklerodermi)

##### Klassifikationskriterier
- Generne optræder i perioder, og der går ofte flere år før diagnosen stilles
- Europæiske og amerikanske kriterier er ikke de samme
	- Kun 15 % af patienter med diagnosen baseret på europæiske kriterier tilfredsstiller de amerikanske kriterier
- For at stille diagnosen Sjögrens syndrom skal 4 af 6 følgende kriterier være opfyldt:
	- Oplysninger om tørre øjne
	- Oplysninger om tør mund
	- Påvisning af nedsat tåreproduktion
	- Positiv spytkirtelbiopsi fra læben
	- Fund af unormalt fungerende spytkirtel
	- Påvisning af autoantistoffer (ANA, SSA, SSB eller RF)

###### Nye internationale kriterier
- Øjensymptomer - mindst et af tre skal være opfyldt
	- Vedvarende tørre øjne hver dag i mere end 3 måneder
	- Recidiverende følelse af sand/grus i øjnene
	- Brug af kunstige tårer mere end tre gange om dagen
- Orale symptomer - mindst et af tre skal være opfyldt
	- Følelse af tør mund hver dag i mindst 3 måneder
	- Recidiverende følelse af hævede spytkirtler hos en voksen person
	- Behov for at drikke for at kunne synke tør mad
- Objektive tegn på tørre øjne - mindst et mål skal være opfyldt
	- Schirmers test
	- Rose-Bengal (Break-up time)
	- Lakrimal kirtelbiopsi med fokusscore > 1
- Objektive tegn på spytkirtelinvolvering - mindst et mål skal være opfyldt
	- Spytkirtelscintigrafi
	- Parotis sialografi
	- Ustimuleret sialometri (=< 1,5 ml per 15 min)
- Laboratoriefund - mindst et skal være positivt
	- Anti-SS-A eller anti-SS-B
	- ANA
	- IgM reumatoid faktor

###### Sekundært Sjögrens syndrom
Kræver en diagnostiseret bindevævssygdom og et sicca-symptom + to positive objektive test på tør mund og øjne

#### Forekomst
Prævalens ca. 0,5%, 90% er kvinder. 20% af ptt. med RA eller SLE har også sekundært SS.

#### Ætiologi
Ukendt

#### Patogenese
Associeret til HLA-DR3 eller -DR5 afhængig af etnicitet. Hypergammaglobinæmi sammen med autoantistoffer tyder på B-lymfocythyperreaktivitet.

#### Symptomer og kliniske fund
- Mundtørhed (*xerostomi*)
	- Svie, smerte
	- Problemer med at tygge og synke
	- Ændret smag
	- Hæshed, taleforstyrrelser
	- *Caries*
	- Slimhinde er blank og glat, ofte med ulcerationer.
- Spytkirtelsvulst
	- Tidligt: typisk bilateral, gl. parotis
	- Senere: Unilateral, tegn på retrograd infektion
- *Keratoconjunctivitis sicca*: øjentørhed og dertil hørende symptomer 
- Generel eksokrin glandulopati
- Ekstraglandulære manifestationer
	- Konstitutionelle
		- Træthed (evt. invaliderende)
		- Temperaturforhøjelse (oftest subfebrilia)
		- Myalgier, artralgier (influenzalignende)
	-  Periepiteliale manifestaioner
		- Nyrer (renal tubulær acidose type I; kronisk interstitiel nefrit; nefrogen diabetes insipidus)
		- Lunger (obstruktiv bronchiolit; interstitiel pneumonit og fibrose)
		- GI (recidiverende pancreatit; primær biliær cirrose)
		- Thyroidea (autoimmun thyroidit)
	- Extraepitheliale
		- Bevægeapparat (synovit, non-erosiv ved primær SS; sjældent myopati)
		- Hud/kar (Raynauds syndrom; kutan vasculitis med palpabel purpura; recidiverende urticaria)
		- Nyrer (membranoproliferativ glomerulonefrit)
		- Nervesystem (kraniel eller perifer polyneuropati; sjældent CNS-involvering)
		- Lymfatisk system (splenomegali og B-celle lymfom [5%])

#### Parakliniske fund
- Let anæmi ("kronisk sygdom"), evt. leukopeni
- Akutfasereaktanter: *Kun sjældent forhøjede*
	- SR kan være øget grundet hypergammaglobinæmi
- Autoantistoffer
	- **anti-SSA, anti-SSB**
	- Reumafaktor (IgA, IgM)
	- ANA
- Oftalmologisk
	- Schirmers test: Filtrerpapir i fornix inferior -- måler fugtning. < 5 mm patologisk.
	- M.fl.
- Odontologisk
	- Sialometri
	- Isotopscintigrafi
	- Slimhindebiopsi

#### Forløb og prognose
Sygdommen øger ikke mortalitet bortset fra øget forekomst af maligne lymfomer (5-10%) og øget infektionsrisiko.

#### Diagnose og differentialdiagnoser
Diagnosen stilles ud fra kriterierne, se `Definition, karakteristik og klassifikation`.

##### Differentialdiagnoser 
er

- Virusinduceret spytkirtelforstørrelse
	- Akutte vira 
		- parotitis
		- coxsackie
		- echo
		- EBV
	- Kroniske vira 
		- hep C
		- HIV
- Amyloidose
- Malignt lymfom
- Sarcoidose
- Tuberkulose
- Kronisk sialoadenitis
- Svampeinfektioner
- Aktinomykose

Non-inflammatorisk spytkirtelforstørrelse kan ses hos patienter med

- kronisk alkoholisme
- levercirrose
- diabetes mellitus
- NSAID-behandling

Ensidig parototis-svulst: overvej infektion.

#### Behandling
Substitution med kunstige tåre og spyt. Kontrol hos tandlæge. For øvrige detaljer, se s. 435.

