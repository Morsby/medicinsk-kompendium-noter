# Introduktion
Disse noter er taget til Medicinsk Kompendium, 18. udgave.

Noterne er skrevet med Markdown-syntax (i `MacDown` på Mac og `Texpad` på iPad), parset af
`pandoc` med hjælp fra `pdftex`, hvorved outputtet er \LaTeX \ og dermed *indbegrebet af smukt*.

Det er forsøgt at tage omfangsrige noter uden dog at skrive bogen af (noget jeg imidlertid har for vane at ende med at gøre alligevel). Det er altså mere end sandsynligt, at noterne her ender med at være omtrent så omfangsrige som Kompendiet.

Noterne tages som en hjælp til mig selv (og er taget på mine præmisser). Enhver anden (talking to you, *Slumdawg*), der benytter noterne, gør dette på **eget ansvar**. Jeg frasiger mig herved enhver form for ansvar for dumpede eksaminer og dræbte patienter (prioriteret rækkefølge).

Noternes udvikling kan følges på [Bitbucket](https://bitbucket.org/Morsby/medicinsk-kompendium-noter/commits/all).

\hspace{20 mm}
\begin{flushright}
Morsby

Aarhus, 15. marts 2015
\end{flushright}

\hspace{20 mm}
\noindent
Seneste revision: \today
