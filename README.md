# Readme

Dette er git-repositoriet for mine noter til Medicinsk Kompendium, 18. udgave.

Her kan løbende følges med i, hvilke ændringer, der udføres. At Commit-beskederne er meningsfulde hver gang, vil jeg ikke ligefrem love - men ikke desto mindre kan ændringer følges.

Den nyeste udgave af noterne i PDF-format kan altid ses på [Google Drev](http://goo.gl/q9eVsW)